<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use AppBundle\Entity\User;

class CreateOrEditUser extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username',TextType::class, [
            'constraints' => [
                new NotBlank(),
            ]
        ])->add('password',TextType::class, [])
        ->add('email',EmailType::class, [
            'constraints' => [
                new NotBlank(),
                new Email(),
            ]
        ])->add('roles',TextType::class, [
            'constraints' => [
                new NotBlank(),
                new Choice([
                    'choices' => [
                        [User::$ROLE_USER],
                        [User::$ROLE_ADMIN],
                        [User::$ROLE_AGENT],
                        [User::$ROLE_PARTNER],
                        [User::$ROLE_TRAINER],
                    ],
                ]),
            ]
        ])->add('isActive',CheckboxType::class)
        ->add('phone',NumberType::class)
        ->add('firstName',TextType::class)
        ->add('lastName',TextType::class)
        ->add('contactEmail',EmailType::class, [
            'constraints' => [
                new Email(),
            ]
        ])->add('contactPhone',NumberType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'empty_data' =>  function (FormInterface $form) { 
                return new User($form->get('username')->getData(), $form->get('email')->getData());
            },
            'constraints' => array(
                new UniqueEntity(array(
                    'fields'=>array('username'),
                    'message'=>'Podany login jest już zajęty.',
                    
                )),
                new UniqueEntity(array(
                    'fields'=>array('email'),
                    'message'=>'Konto z tym adresem e-mail już istnieje.',
                )),
            ),
        ));
    }
}