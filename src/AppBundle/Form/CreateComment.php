<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 02.03.18
 * Time: 20:56
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateComment extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content',TextType::class, [
            'constraints' => [
                new NotBlank(),
            ]
        ]);
    }
}