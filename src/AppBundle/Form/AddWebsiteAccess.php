<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\Choice;

use AppBundle\Entity\User\WebsiteAccess;

class AddWebsiteAccess extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('website',IntegerType::class, [])
        ->add('accessType',TextType::class, [
            'constraints' => [
                new Choice([
                    'choices' => [
                        WebsiteAccess::KAM,
                        WebsiteAccess::TRAINER,
                        WebsiteAccess::OWNER,
                        WebsiteAccess::AGENT,
                    ],
                ]),
            ]
        ])
        ->add('isPrimary',CheckboxType::class, []);
    }
}