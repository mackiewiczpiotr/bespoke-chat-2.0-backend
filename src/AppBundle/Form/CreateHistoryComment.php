<?php
/**
 * Created by PhpStorm.
 * User: bartoszskuza
 * Date: 09/04/2018
 * Time: 17:51
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateHistoryComment extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content',TextType::class, [
            'constraints' => [
                new NotBlank(),
            ]
        ]);
    }
}