<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePassword extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('oldPassword',TextType::class, [
            'constraints' => [
                new NotBlank(),
            ]
        ])->add('newPassword',RepeatedType::class, [
            'constraints' => [
                new NotBlank(),
            ],
            'type'=>PasswordType::class,
            'first_name'=>'first',
            'second_name'=>'second',
        ]);
    }
}