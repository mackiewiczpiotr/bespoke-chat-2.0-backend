<?php
/**
 * Created by PhpStorm.
 * User: bartoszskuza
 * Date: 02/06/2018
 * Time: 18:09
 */

namespace AppBundle\Exception;


use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AccountDisabledException extends AccountStatusException
{

}