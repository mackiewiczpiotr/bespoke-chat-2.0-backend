<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 03.12.17
 * Time: 11:17
 */

namespace AppBundle\Pagination;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Paginator
{
    public static $_DEFAULT_LIMIT = 25;
    /**
     * @var QueryBuilder
     */
    private $qb;

    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var string[]
     */
    private $sortableFields;

    /**
     * @var Filter[]
     */
    private $filters;


    /**
     * Paginator constructor.
     * @param QueryBuilder $qb
     * @param Mapper $mapper
     */
    public function __construct(QueryBuilder $qb, Mapper $mapper, $sortableFields)
    {
        $this->qb = $qb;
        $this->mapper = $mapper;
        $this->filters = [];
        $this->sortableFields = $sortableFields;

    }


    public function paginate(ParamFetcher $paramFetcher)
    {
        $page = $paramFetcher->get('page');
        $userLimit = $paramFetcher->get('limit');
        $sortField = $paramFetcher->get('sortField');
        $sortOrder = $paramFetcher->get('sortOrder');

        $limit = $userLimit > 0 ? $userLimit : static::$_DEFAULT_LIMIT;

        $this->slice($page, $limit);
        $this->sort($sortField, $sortOrder);
        $this->filter($paramFetcher);


        $doctrinePaginator = new DoctrinePaginator($this->qb);
        $count = $doctrinePaginator->count();

        return [
            'data' => $this->qb->getQuery()->getResult(),
            'pagination' => [
                'count' => $count,
                'pages' => $this->calculatePageNumbers($count, $limit)
            ]
        ];
    }



    private function slice($page, $limit)
    {
        $this->qb
            ->setFirstResult($limit * ($page - 1))->setMaxResults($limit);
    }

    private function calculatePageNumbers($count, $limit)
    {
        return ceil($count / $limit);
    }

    private function sort($sortField, $sortOrder)
    {
        $column = $this->mapper->getDbField($sortField);
        if (in_array($sortField, $this->sortableFields) && !is_null($column)) {
            $this->qb->addOrderBy($column, $sortOrder);
        }
    }

    private function filter(ParamFetcher $paramFetcher)
    {
        foreach ($this->filters as $filter) {
            $column = $this->mapper->getDbField($filter->getMappingColumn());
            if (!is_null($column)) {
                $value = $paramFetcher->get($filter->getParam());
                $filter->add($this->qb, $column, $value);
            }

        }
    }


    /**
     * @param Filter[] $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    /**
     * @param $filter
     * @return $this
     */
    public function addFilter($filter)
    {
        $this->filters[] = $filter;
        return $this;
    }

    /**
     * TODO consider return cloned queryBuilder instead of reference
     * @return QueryBuilder
     */
    public function getQb()
    {
        return $this->qb;
    }



}