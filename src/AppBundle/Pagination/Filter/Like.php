<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 03.12.17
 * Time: 11:02
 */

namespace AppBundle\Pagination\Filter;


use AppBundle\Pagination\Filter;
use Doctrine\ORM\QueryBuilder;

class Like extends Filter
{
    public function add(QueryBuilder $qb, $column, $value)
    {
        if ($value !== '') {
            $alias = ":".$this->getParam();
            $qb->andWhere($qb->expr()->like($column, $alias))
                ->setParameter($alias, '%'.$value . '%');
        }
    }
}