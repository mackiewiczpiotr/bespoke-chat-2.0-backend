<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 15.12.17
 * Time: 21:53
 */

namespace AppBundle\Pagination\Filter;


use AppBundle\Pagination\Filter;
use DateTime;
use Doctrine\ORM\QueryBuilder;

class Date extends Filter
{
    const AFTER = 'after';
    const AFTER_OR_EQUAL = 'after-or-equal';
    const BEFORE = 'before';
    const BEFORE_OR_EQUAL = 'before-or-equal';
    const DATE_FORMAT = 'Y-m-d';

    private $type;

    /**
     * Date constructor.
     * @param $type
     */
    public function __construct($mappingColumn, $param,$type)
    {
        parent::__construct($mappingColumn, $param);
        $this->type = $type;
    }

    public function add(QueryBuilder $qb, $column, $value)
    {

        $alias = ':'.$this->getParam();
        $date = $this->convertToDate($value);
        $expression = $this->getComparisonExpression($qb,$column,$alias);
        if(!is_null($date) && !is_null($expression)){
            $qb->andWhere($expression)
                ->setParameter($alias,$date->format(self::DATE_FORMAT));
        }
    }

    private function getComparisonExpression(QueryBuilder $qb,$column,$alias){
        switch($this->type){
            case self::AFTER:
                return $qb->expr()->gt($column,$alias);
            case self::AFTER_OR_EQUAL:
                return $qb->expr()->gte($column,$alias);
            case self::BEFORE:
                return $qb->expr()->lt($column,$alias);
            case self::BEFORE_OR_EQUAL:
                return $qb->expr()->lte($column,$alias);
            default:
                return null;
        }
    }

    private function convertToDate($value)
    {
        if ($value !== '') {
            $date = DateTime::createFromFormat(self::DATE_FORMAT, $value);
            if ($date !== false) {
                return $date;
            }
        }
        return null;
    }
}