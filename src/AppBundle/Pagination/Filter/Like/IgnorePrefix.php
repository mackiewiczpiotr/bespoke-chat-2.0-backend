<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 03.12.17
 * Time: 11:02
 */

namespace AppBundle\Pagination\Filter\Like;


use AppBundle\Pagination\Filter\Like;
use Doctrine\ORM\QueryBuilder;

class IgnorePrefix extends Like
{
    private $prefix;

    /**
     * Filter constructor.
     * @param $mappingColumn
     * @param $param
     * @param $prefix
     */
    public function __construct($mappingColumn, $param, $prefix)
    {
        $this->prefix = $prefix;
        parent::__construct($mappingColumn, $param);
    }

    public function add(QueryBuilder $qb, $column, $value)
    {
        if ($value !== '') {
            $parsedValue = preg_replace('/^' . preg_quote($this->prefix, '/') . '/', '', $value);
            parent::add($qb, $column, $parsedValue);
        }
    }
}