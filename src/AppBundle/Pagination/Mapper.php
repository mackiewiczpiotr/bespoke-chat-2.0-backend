<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 03.12.17
 * Time: 10:08
 */

namespace AppBundle\Pagination;


class Mapper
{
    /**
     * @var array
     */
    private $data;

    /**
     * Mapper constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }


    public function getDbField($alias){
        if(isset($this->data[$alias])){
            return $this->data[$alias];
        }
        return null;
    }

}