<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 03.12.17
 * Time: 10:21
 */

namespace AppBundle\Pagination;


use AppBundle\Pagination\Filter\Expression;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class Filter
{
    /**
     * @var string;
     */
    private $mappingColumn;

    private $param;

    /**
     * Filter constructor.
     * @param $mappingColumn
     * @param $param
     */
    public function __construct($mappingColumn, $param)
    {
        $this->mappingColumn = $mappingColumn;
        $this->param = $param;
    }


    public abstract function add(QueryBuilder $qb, $column, $value);

    /**
     * @return string
     */
    public function getMappingColumn()
    {
        return $this->mappingColumn;
    }

    /**
     * @return mixed
     */
    public function getParam()
    {
        return $this->param;
    }




}