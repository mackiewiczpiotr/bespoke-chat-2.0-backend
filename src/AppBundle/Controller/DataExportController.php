<?php
namespace AppBundle\Controller;

use AppBundle\Repository\WebsiteRepository;
use AppBundle\Service\DataExportService;
use AppBundle\DTO\FileExportDetails;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;

class DataExportController extends FOSRestController
    implements ClassResourceInterface
{
    /**
     * @var WebsiteRepository
     */
    private $websiteRepository;
    /**
     * @var DataExportService
     */
    private $dataExportService;

    /**
     * DataExportController constructor.
     * @param WebsiteRepository $websiteRepository
     * @param DataExportService $dataExportService
     */
    public function __construct(WebsiteRepository $websiteRepository, DataExportService $dataExportService)
    {
        $this->websiteRepository = $websiteRepository;
        $this->dataExportService = $dataExportService;
    }

    /**
     * @Get("/website/{id}/data-export")
     * @View
     */
    public function getAction(Request $request, $id)
    {
        $website = $this->websiteRepository->find($id);
        $this->denyAccessUnlessGranted('access', $website, WebsiteController::ERROR_MSG);
        $exportParams = new FileExportDetails();
        $exportParams->website = $website;
        $exportParams->params = $request->query->all();
        $exportParams->type = $request->get('type');
        return $this->dataExportService->getData($exportParams);
    }
}