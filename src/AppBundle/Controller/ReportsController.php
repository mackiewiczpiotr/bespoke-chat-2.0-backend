<?php
/**
 * Created by PhpStorm.
 * User: bartoszskuza
 * Date: 27/12/2017
 * Time: 17:23
 */

namespace AppBundle\Controller;


use AppBundle\Pagination\Filter\Date;
use AppBundle\Pagination\Mapper;
use AppBundle\Pagination\Paginator;
use AppBundle\Repository\ChatReportRepository;
use AppBundle\Repository\WebsiteRepository;
use AppBundle\Service\DateRangeCalculator;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Routing\ClassResourceInterface;
use AppBundle\Pagination\Filter;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class ReportsController extends FOSRestController
    implements ClassResourceInterface
{
    /**
     * @var WebsiteRepository
     */
    private $websiteRepository;

    /**
     * @var ChatReportRepository
     */
    private $chatReportsRepository;

    /**
     * @var DateRangeCalculator
     */
    private $dateRangeCalculator;


    public function __construct(WebsiteRepository $websiteRepository, ChatReportRepository $chatReportsRepository,
                                DateRangeCalculator $dateRangeCalculator)
    {
        $this->websiteRepository = $websiteRepository;
        $this->chatReportsRepository = $chatReportsRepository;
        $this->dateRangeCalculator = $dateRangeCalculator;
    }

    /**
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="limit", requirements="\d+", strict=true, default="20", description="Item per page limit")
     * @QueryParam(name="sortField", requirements="[a-z|A-Z]+", allowBlank=false, default="created", description="Sort field")
     * @QueryParam(name="sortOrder", requirements="(asc|desc)", allowBlank=false, default="asc", description="Sort direction")
     * @QueryParam(name="id", requirements="[a-z|A-Z|\d|\#]+", allowBlank=true, description="Filter by client name")
     * @QueryParam(name="clientName", requirements="[a-z|A-Z| ]+", allowBlank=true, description="Filter by client name")
     * @QueryParam(name="clientPhone", requirements="[a-zA-Z0-9 \-]+", allowBlank=true, description="Filter by livechat id")
     * @QueryParam(name="clientEmail", requirements="[a-zA-Z0-9@.\-]+", allowBlank=true, description="Filter by livechat id")
     * @QueryParam(name="startDate", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="endDate", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started before")
     * @QueryParam(name="reportType", requirements=".*", allowBlank=true, description="Filter by report type name")
     */
    public function cgetAction(ParamFetcher $paramFetcher, $slug)
    {
        $website = $this->websiteRepository->find($slug);
        $this->denyAccessUnlessGranted('access', $website, 'Website not found or you don\'t have access to it');

        $baseQuery = $this->chatReportsRepository->getBelongToWebsiteQuery($slug);
        $mapper = new Mapper([
            'id' => 'r.id',
            'created' => 'r.created',
            'clientName' => 'r.clientName',
            'clientPhone' => 'r.clientPhone',
            'clientEmail' => 'r.clientEmail',
            'reportType' => 't.name'
        ]);
        $paginator = new Paginator($baseQuery, $mapper, ['id', 'created', 'clientName', 'clientPhone', 'clientEmail']);
        $clientNameFilter = new Filter\Like('clientName', 'clientName');
        $clientPhoneFilter = new Filter\Like('clientPhone', 'clientPhone');
        $clientEmailFilter = new Filter\Like('clientEmail', 'clientEmail');
        $reportTypeFilter = new Filter\Like('reportType', 'reportType');
        $staredAfter = new Date('created','startDate',Date::AFTER_OR_EQUAL);
        $staredBefore = new Date('created','endDate',Date::BEFORE_OR_EQUAL);
        $idFilter = new Filter\Like\IgnorePrefix('id', 'id','#');

        $paginator->addFilter($clientNameFilter)
            ->addFilter($clientEmailFilter)
            ->addFilter($clientPhoneFilter)
            ->addFilter($idFilter)
            ->addFilter($reportTypeFilter)
            ->addFilter($staredBefore)
            ->addFilter($staredAfter);

        $data = $paginator->paginate($paramFetcher);

        $dataRange = $this->dateRangeCalculator->calculate('r.created',$paginator->getQb());

        $view = $this->view(array_merge($data,$dataRange), 200);
        
        $view->getContext()->setGroups([
            'Default',
            'List',
            'chat' => ['Id'],
            'reportType' =>['Basic'],
        ]);

        return $view;
    }
}