<?php
namespace AppBundle\Controller;

use AppBundle\Service\WebsiteAccessService;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\WebsiteRepository;
use AppBundle\Repository\WebsiteAccessRepository;
use AppBundle\Form\AddWebsiteAccess;
use AppBundle\Form\User\Create;
use AppBundle\Form\User\Edit;
use AppBundle\Pagination\Mapper;
use AppBundle\Pagination\Paginator;
use AppBundle\Pagination\Filter;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("website-access")
 */
class WebsiteAccessController extends FOSRestController
    implements ClassResourceInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var WebsiteRepository
     */
    private $websiteRepository;

    /**
     * @var WebsiteAccessService
     */
    private $websiteAccessService;

    /**
     * @var WebsiteAccessRepository
     */
    private $websiteAccessRepository;


    /**
     * UsersController constructor.
     * @param UserRepository $userRepository
     * @param WebsiteRepository $websiteRepository
     * @param WebsiteAccessService $websiteAccessService
     * @param WebsiteAccessRepository $websiteAccessRepository
     */
    function __construct(
        UserRepository $userRepository, 
        WebsiteRepository $websiteRepository, 
        WebsiteAccessService $websiteAccessService,
        WebsiteAccessRepository $websiteAccessRepository)
    {
        $this->userRepository = $userRepository;
        $this->websiteRepository = $websiteRepository;
        $this->websiteAccessService = $websiteAccessService;
        $this->websiteAccessRepository = $websiteAccessRepository;
    }

    /**
     * @View
     */
    public function getAction($id)
    {
        return $this->websiteAccessRepository->getUserAccess($this->userRepository->find($id));
    }

    /**
     * @View
     * @return array|\Symfony\Component\Form\Form
     */
    public function postAction(Request $request, $id)
    {
        $form = $this->createForm(AddWebsiteAccess::class);
        $form->submit($request->request->all());

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $this->userRepository->find($id);
            $website = $this->websiteRepository->find($data['website']);
            $type = $data['accessType'];
            $isPrimary = $data['isPrimary'];
            return $this->websiteAccessService->add($user, $website, $type, $isPrimary);
        }
        return $form;
    }

    /**
     * @View
     */
    public function deleteAction($userId, $accessId)
    {
        $user = $this->userRepository->find($userId);
        return $this->websiteAccessService->remove($accessId, $user);
    }

}