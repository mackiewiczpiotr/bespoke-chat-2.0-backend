<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 23.01.18
 * Time: 21:11
 */

namespace AppBundle\Controller;


use AppBundle\Repository\WebsiteRepository;
use AppBundle\Service\StatsService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;

class StatsController extends FOSRestController
    implements ClassResourceInterface
{
    /**
     * @var WebsiteRepository
     */
    private $websiteRepository;
    /**
     * @var StatsService
     */
    private $statsService;

    /**
     * StatsController constructor.
     * @param WebsiteRepository $websiteRepository
     */
    public function __construct(WebsiteRepository $websiteRepository, StatsService $statsService)
    {
        $this->websiteRepository = $websiteRepository;
        $this->statsService = $statsService;
    }

    /**
     * @View
     * @QueryParam(name="from", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="to", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="groupBy", requirements="(hours|)", allowBlank=false, default="", description="Started after")
     */
    public function getTotalAction(ParamFetcher $paramFetcher, $id)
    {
        $website = $this->websiteRepository->find($id);
        $this->denyAccessUnlessGranted('access', $website, WebsiteController::ERROR_MSG);
        $byHours = $paramFetcher->get('groupBy') === 'hours';
        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        return $this->statsService->getTotal($website, $from, $to, $byHours);
    }
    /**
     * @View
     * @QueryParam(name="from", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="to", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     */
    public function getAvailabilityAction(ParamFetcher $paramFetcher, $id){
        $website = $this->websiteRepository->find($id);
        $this->denyAccessUnlessGranted('access', $website, WebsiteController::ERROR_MSG);
        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        return $this->statsService->getAvailability($website, $from, $to);
    }
    /**
     * @View
     * @QueryParam(name="from", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="to", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     */
    public function getEngagementAction(ParamFetcher $paramFetcher, $id){
        $website = $this->websiteRepository->find($id);
        $this->denyAccessUnlessGranted('access', $website, WebsiteController::ERROR_MSG);
        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        return $this->statsService->getEngagement($website, $from, $to);
    }
    /**
     * @View
     * @QueryParam(name="from", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="to", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     */
    public function getRatingsAction(ParamFetcher $paramFetcher, $id){
        $website = $this->websiteRepository->find($id);
        $this->denyAccessUnlessGranted('access', $website, WebsiteController::ERROR_MSG);
        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        return $this->statsService->getRatings($website, $from, $to);
    }
    /**
     * @View
     * @QueryParam(name="from", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="to", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     */
    public function getGreetingsAction(ParamFetcher $paramFetcher, $id){
        $website = $this->websiteRepository->find($id);
        $this->denyAccessUnlessGranted('access', $website, WebsiteController::ERROR_MSG);
        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        return $this->statsService->getGreetings($website, $from, $to);
    }

    /**
     * @View
     * @QueryParam(name="from", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="to", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="groupBy", requirements="(name|days)", allowBlank=false, default="name", description="Started after")
     */
    public function getRaporttypeAction(ParamFetcher $paramFetcher, $id){
        $website = $this->websiteRepository->find($id);
        $this->denyAccessUnlessGranted('access', $website, WebsiteController::ERROR_MSG);
        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        $byDays = $paramFetcher->get('groupBy') === 'days';
        if($byDays){
            return $this->statsService->getReportTypeDaily($website, $from, $to);
        }
        return $this->statsService->getReportType($website, $from, $to);
    }
}