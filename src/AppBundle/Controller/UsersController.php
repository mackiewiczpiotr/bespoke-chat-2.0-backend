<?php
namespace AppBundle\Controller;

use AppBundle\Service\UserService;
use AppBundle\Repository\UserRepository;
use AppBundle\Form\CreateOrEditUser;
use AppBundle\Pagination\Mapper;
use AppBundle\Pagination\Paginator;
use AppBundle\Pagination\Filter;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends FOSRestController
    implements ClassResourceInterface
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UsersController constructor.
     * @param UserService $userService
     * @param UserRepository $userRepository
     */
    function __construct(
        UserService $userService, 
        UserRepository $userRepository)
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }

    /** 
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="limit", requirements="\d+", strict=true, default="20", description="Item per page limit")
     * @QueryParam(name="sortField", requirements="[a-z|A-Z]+", allowBlank=false, default="id", description="Sort field")
     * @QueryParam(name="sortOrder", requirements="(asc|desc)", allowBlank=false, default="asc", description="Sort direction")
     * @QueryParam(name="id", requirements="\d+", allowBlank=true, description="Filter by user id")
     * @QueryParam(name="username", requirements="[a-z|A-Z| ]+", allowBlank=true, description="Filter by user username")
     * @QueryParam(name="email", requirements="[a-zA-Z0-9@]+", allowBlank=true, description="Filter by user email")
     * @QueryParam(name="roles", requirements="[a-zA-Z_ ]+", allowBlank=true, description="Filter by user role")
     * @QueryParam(name="isActive", requirements="\d+", allowBlank=true, description="Filter by user is_active filed")
     * @View(
     *     serializerGroups={"Default", "Details", "Admin"},
     * )
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $baseQuery = $this->userRepository->getFindAllQuery();
        $mapper = new Mapper([
            'id'=>'u.id',
            'username'=>'u.username',
            'email'=>'u.email',
            'isActive'=>'u.isActive',
        ]);
        $paginator = new Paginator($baseQuery, $mapper, ['id','username','email','isActive']);
        $idFilter = new Filter\Like\IgnorePrefix('id', 'id','#');
        $usernameFilter = new Filter\Like('username', 'username');
        $emailFilter = new Filter\Like('email', 'email');
        $isActiveFilter = new Filter\Like\IgnorePrefix('isActive', 'isActive','#');
        $paginator->addFilter($idFilter)
            ->addFilter($usernameFilter)
            ->addFilter($emailFilter)
            ->addFilter($isActiveFilter);
        $data = $paginator->paginate($paramFetcher);
        return $data;
    }

    /** 
     * @View(
     *     serializerGroups={"Default", "Details", "Contact" ,"Admin"},
     * )
     */
    public function getAction($id)
    {
        return $this->userRepository->find($id);
    }

    /** 
     * @View
     * @return integer|\Symfony\Component\Form\Form
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(CreateOrEditUser::class);
        $form->submit($request->request->all());

        if($form->isSubmitted() && $form->isValid()) {
            $manager =$this->getDoctrine()->getManager();
            $user = $form->getData();
            $password = $this->userService->generatePassword($user, $user->getPassword());
            $user->setPassword($password);
            $manager->persist($user);
            $manager->flush();
            return $user->getId();

        }
        return $form;
    }

    /**
     * @View(
     *     serializerGroups={"Default", "Details", "Contact" ,"Admin"},
     * )
     * @param Request $request
     * @param $id
     * @return User|\Symfony\Component\Form\Form
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function patchAction(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        $oldPassword = $user->getPassword();
        $form = $this->createForm(CreateOrEditUser::class, $user);
        $form->submit($request->request->all());
        if($form->isSubmitted() && $form->isValid())
        {
            $updatedUser = $this->userService->updatePassword($form->getData(), $oldPassword);
            $manager =$this->getDoctrine()->getManager();
            $manager->persist($updatedUser);
            $manager->flush();
            return $updatedUser;
        }
        return $form;
    }
}