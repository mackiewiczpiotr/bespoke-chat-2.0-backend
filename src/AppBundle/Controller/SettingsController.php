<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\ChangePassword;

class SettingsController extends FOSRestController
    implements ClassResourceInterface
{

    /**
     * SettingsController constructor.
     */
    function __construct()
    {
        
    }

    /**
     * @View(
     *     serializerGroups={"Default", "Details"},
     * )
     */
    public function cgetAction()
    {
        return $this->getUser();
    }

    /**
     * @View
     * @return boolean|\Symfony\Component\Form\Form
     */
    public function patchPasswordAction(Request $request)
    {
        $form = $this->createForm(ChangePassword::class);
        $user = $this->getUser();
        if(password_verify($request->get('oldPassword'), $user->getPassword()))
        {
            $form->submit($request->request->all());
            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $encoder = $this->container->get('security.password_encoder');
                $password = $encoder->encodePassword($user, $data['newPassword']);
                $user->setPassword($password);
                $manager =$this->getDoctrine()->getManager();
                $manager->persist($user);
                $manager->flush();
                return true;
            }
            return $form;
        }else
            return ['children'=>['oldPassword'=>['errors'=>['Nieprawidłowe hasło']]]];
    }
}