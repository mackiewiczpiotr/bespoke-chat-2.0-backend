<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 30.11.17
 * Time: 19:48
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Chat;
use AppBundle\Entity\User;
use AppBundle\Form\CreateComment;
use AppBundle\Pagination\Filter;
use AppBundle\Pagination\Filter\Date;
use AppBundle\Pagination\Mapper;
use AppBundle\Pagination\Paginator;
use AppBundle\Repository\ChatHistoryRepository;
use AppBundle\Repository\ChatReportRepository;
use AppBundle\Repository\ChatRepository;
use AppBundle\Repository\WebsiteRepository;

use AppBundle\Service\ChatService;
use AppBundle\Service\DateRangeCalculator;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ChatController extends FOSRestController
    implements ClassResourceInterface
{

    /**
     * @var WebsiteRepository
     */
    private $websiteRepository;
    /**
     * @var ChatRepository
     */
    private $chatRepository;
    /**
     * @var ChatReportRepository
     */
    private $chatReportRepository;

    /**
     * @var ChatHistoryRepository
     */
    private $chatHistoryRepository;
    /**
     * @var ChatService
     */
    private $chatService;

    /**
     * @var DateRangeCalculator
     */
    private $dateRangeCalculator;

    public function __construct(
        WebsiteRepository $websiteRepository,
        ChatRepository $chatRepository,
        ChatReportRepository $chatReportRepository,
        ChatHistoryRepository $chatHistoryRepository,
        ChatService $chatService,
        DateRangeCalculator $dateRangeCalculator)
    {
        $this->websiteRepository = $websiteRepository;
        $this->chatRepository = $chatRepository;
        $this->chatReportRepository = $chatReportRepository;
        $this->chatHistoryRepository = $chatHistoryRepository;
        $this->chatService = $chatService;
        $this->dateRangeCalculator = $dateRangeCalculator;

    }


    /**
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="limit", requirements="\d+", strict=true, default="20", description="Item per page limit")
     * @QueryParam(name="sortField", requirements="[a-z|A-Z]+", allowBlank=false, default="id", description="Sort field")
     * @QueryParam(name="sortOrder", requirements="(asc|desc)", allowBlank=false, default="asc", description="Sort direction")
     * @QueryParam(name="clientName", requirements="[a-z|A-Z| ]+", allowBlank=true, description="Filter by client name")
     * @QueryParam(name="livechatId", requirements="[a-zA-Z0-9]+", allowBlank=true, description="Fiter by livechat id")
     * @QueryParam(name="startDate", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started after")
     * @QueryParam(name="endDate", requirements="[1-9]\d{3}\-\d{2}\-\d{2}", allowBlank=true, description="Started before")
     * @QueryParam(name="tags", requirements=".*", allowBlank=true, description="Filter by tag label")
     * @View
     */
    public function cgetAction(ParamFetcher $paramFetcher, $slug)
    {
        $website = $this->websiteRepository->find($slug);
        $this->denyAccessUnlessGranted('access', $website, 'Website not found or you don\'t have access to it');

        $baseQuery = $this->chatRepository->getBelongToWebsite($slug);
        $mapper = new Mapper([
            'id' => 'c.id',
            'livechatId' => 'c.livechatId',
            'clientName' => 'c.clientName',
            'spentTime' => 'c.spentTime',
            'startTime' => 'c.startTime',
            'tags' =>'t.label'
        ]);
        $paginator = new Paginator($baseQuery, $mapper, ['id', 'livechatId', 'clientName', 'spentTime', 'startTime']);
        $clientNameFilter = new Filter\Like('clientName', 'clientName');
        $liveChatFilter = new Filter\Like('livechatId', 'livechatId');
        $tagFilter = new Filter\Like('tags', 'tags');
        $staredAfter = new Date('startTime', 'startDate', Date::AFTER_OR_EQUAL);
        $staredBefore = new Date('startTime', 'endDate', Date::BEFORE_OR_EQUAL);
        $paginator->addFilter($clientNameFilter)
            ->addFilter($liveChatFilter)
            ->addFilter($tagFilter)
            ->addFilter($staredBefore)
            ->addFilter($staredAfter);
        $data = $paginator->paginate($paramFetcher);

        $qb = $paginator->getQb();
        $qb->setMaxResults(1)->setFirstResult(0);
        $qb->resetDQLParts(['select','groupBy']);
        $qb->addSelect($qb->expr()->max('c.startTime'))
            ->addSelect($qb->expr()->min('c.startTime'));

        $dataRange = $this->dateRangeCalculator->calculate('c.startTime',$paginator->getQb());

        $view = $this->view(array_merge($data,$dataRange), 200);
        $view->getContext()->setGroups([
            'Default',
            'reports'=>['Name','Id']
        ]);

        return $view;
    }

    /**
     * @param $websiteId
     * @param $livechatId
     * @return null|object
     * @View(
     *     serializerGroups={"Default", "Details"},
     * )
     */
    public function getAction($websiteId, $livechatId)
    {
        $website = $this->websiteRepository->find($websiteId);
        $this->denyAccessUnlessGranted('access', $website, 'Website not found or you don\'t have access to it');
        $chat = $this->chatRepository->findOneBy(array('livechatId' => $livechatId));
        if (is_null($chat) || $chat->getWebsite()->getId() !== $website->getId()) {
            throw new AccessDeniedException('Chat not found or you don\'t have access to it');
        }
        return $chat;
    }

    /**
     * @param $websiteId
     * @param $chatId
     * @View
     * @return \FOS\RestBundle\View\View
     */
    public function getHistoryAction($websiteId, $chatId)
    {
        $chat = $this->getChatFromRequest($websiteId, $chatId);
        $data =  $this->chatService->getChatHistory($chat, $this->getUser());

        $view = $this->view($data, 200);
        $serializerGroup = ["Default", "Details"];
        if(!in_array(User::$ROLE_PARTNER,$this->getUser()->getRoles())){
            $serializerGroup[] = "Comments";
        }

        $view->getContext()->setGroups($serializerGroup);
        
        return $view;
    }

    /**
     * @param $websiteId
     * @param $chatId
     * @return array
     * @View
     */
    public function getCommentsAction($websiteId, $chatId)
    {
        $chat = $this->getChatFromRequest($websiteId, $chatId);
        return $this->chatService->getChatComments($chat);
    }


    /**
     * @param Request $request
     * @param $websiteId
     * @param $chatId
     * @View(
     *      serializerGroups={"Default","Author"}
     *     )
     * @return Chat\Comment|\Symfony\Component\Form\Form
     */
    public function postCommentAction(Request $request, $websiteId, $chatId)
    {
        $chat = $this->getChatFromRequest($websiteId, $chatId);
        $form = $this->createForm(CreateComment::class);
        $form->submit($request->request->all());

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $comment =new Chat\Comment($chat,$this->getUser(),$data['content']);
            $manager =$this->getDoctrine()->getManager();
            $manager->persist($comment);
            $manager->flush();
            return $comment;
        }
        return $form;
    }

    /**
     * @param $websiteId
     * @param $chatId
     * @return array
     * @View(
     *      serializerGroups={"Default","Details"}
     *     )
     */
    public function getReportsAction($websiteId, $chatId)
    {
        $chat = $this->getChatFromRequest($websiteId, $chatId);
        return $this->chatReportRepository->getBelongToChat($chatId);
    }

    /**
     * @param $websiteId
     * @param $chatId
     * @return array
     * @View(
     *      serializerGroups={"Id"}
     *     )
     */
    public function getChatswiththesameclientAction($websiteId, $chatId)
    {
        $chat = $this->getChatFromRequest($websiteId, $chatId);
        return $this->chatRepository->getChatsWithTheSameClient($chat);
    }

    /**
     * @param $websiteId
     * @param $chatId
     * @return Chat
     */
    private function getChatFromRequest($websiteId, $chatId)
    {
        $website = $this->websiteRepository->find($websiteId);
        $this->denyAccessUnlessGranted('access', $website, 'Website not found or you don\'t have access to it');
        $chat = $this->chatRepository->find($chatId);
        if (is_null($chat) || $chat->getWebsite()->getId() !== $website->getId()) {
            throw new AccessDeniedException('Chat not found or you don\'t have access to it');
        }
        return $chat;
    }

    private function getChatHistoryFromRequest($websiteId,$chatId,$historyId){
        $website = $this->websiteRepository->find($websiteId);
        $this->denyAccessUnlessGranted('access', $website, 'Website not found or you don\'t have access to it');
        $chat = $this->chatRepository->find($chatId);
        if (is_null($chat) || $chat->getWebsite()->getId() !== $website->getId()) {
            throw new AccessDeniedException('Chat not found or you don\'t have access to it');
        }
        $history = $this->chatHistoryRepository->find($historyId);
        if(is_null(($history)) || $history->getChat()->getId() !== $chat->getId()){
            throw new AccessDeniedException('History not found or you don\'t have access to it');
        }

        return $history;
    }
}