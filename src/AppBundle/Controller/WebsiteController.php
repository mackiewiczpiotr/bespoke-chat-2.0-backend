<?php
/**
 * Created by PhpStorm.
 * User: bartosz.skuza
 * Date: 2017-11-02
 * Time: 16:00
 */

namespace AppBundle\Controller;

use AppBundle\DTO\WebsiteDetails;
use AppBundle\Repository\ChatCommentRepository;
use AppBundle\Repository\ChatRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\WebsiteRepository;
use AppBundle\Service\UserService;
use AppBundle\Service\WebsiteService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Route;


class WebsiteController extends FOSRestController
    implements ClassResourceInterface
{
    const ERROR_MSG = 'Website not found or you don\'t have access to it';
    /**
     * @var WebsiteService
     */
    private $websiteService;

    /**
     * @var WebsiteRepository
     */
    private $websiteRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var ChatRepository
     */
    private $chatRepository;

    /**
     * @var ChatCommentRepository
     */
    private $chatCommentRepository;


    /**
     * WebsiteController constructor.
     * @param WebsiteService $websiteService
     * @param WebsiteRepository $websiteRepository
     * @param ChatRepository $chatRepository
     * @param ChatCommentRepository $chatCommentRepository
     * @param UserService $userService
     * @param UserRepository $userRepository
     */
    public function __construct(
        WebsiteService $websiteService,
        WebsiteRepository $websiteRepository,
        ChatRepository $chatRepository,
        ChatCommentRepository $chatCommentRepository,
        UserService $userService,
        UserRepository $userRepository
    )
    {
        $this->websiteService = $websiteService;
        $this->websiteRepository = $websiteRepository;
        $this->chatRepository = $chatRepository;
        $this->chatCommentRepository = $chatCommentRepository;
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route(requirements={"id"="\d+"})
     */
    public function getAction($id)
    {
        $website = $this->websiteRepository->find($id);

        $this->denyAccessUnlessGranted('access', $website, self::ERROR_MSG);
        $data = new WebsiteDetails();
        $data->details = $website;
        $data->agents = $this->userRepository->getWebsiteTeam($website);
        $chartData = $this->chatRepository->getNumberOfChatsGroupedByDaysFromWeek($website);

        $data->chartData = $chartData;
        $data->totalChats = $this->chatRepository->getTotalNumberOfChatsFromWeek($website);
        $data->trainers = $this->userRepository->getTrainers($website);
        $data->admins = $this->userRepository->getAdmins($website);
        $view = $this->view($data, 200);
        $view->getContext()->setGroups([
                'Default',
                'details' => ['Default', 'Details'],
                'admins' => ['Default', 'Contact'],
                'trainers' => ['Default', 'Contact']
            ]
        );
        return $view;
    }

    public function getPraisesAction($id){
        $website = $this->websiteRepository->find($id);

        $this->denyAccessUnlessGranted('access', $website, self::ERROR_MSG);

        $data = $this->chatCommentRepository->getPraisesForWebsite($id);
        $view = $this->view($data, 200);
        $view->getContext()->setGroups([
            'Praise',
            'Id'
            ]
        );
        return $view;
    }

    /** 
     * @View
     */
    public function cgetAction()
    {
        return $this->websiteService->getAvailable($this->getUser());
    }

    /**
     * @View
     */
    public function cgetAvailableAction()
    {
        $lastWorkingContext = $this->userService->getWorkingContext($this->getUser());
        $response = [
            'websites' => $this->websiteService->getAvailable($this->getUser()),
        ];
        if (!is_null($lastWorkingContext)) {
            $response['workingContext'] = $lastWorkingContext->getId();
        }
        return $response;
    }

    /**
     * @View
     */
    public function patchWorkingcontextAction($id)
    {
        $website = $this->websiteRepository->find($id);
        $this->denyAccessUnlessGranted('access', $website, self::ERROR_MSG);

        $user = $this->getUser();
        $user->setLastWorkingContext($website);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();
    }
}
