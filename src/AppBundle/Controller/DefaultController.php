<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Chat\Comment;
use AppBundle\Entity\Website;
use Doctrine\Common\Util\Debug;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/api/getToken")
     * @Method({"POST"})
     */
    public function getTokenAction()
    {
        return new Response('', 401);
    }

    /**
     * @Route("/api/getSecret")
     * @Method({"GET"})
     */
    public function getSecretAction()
    {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
    }
}
