<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use AppBundle\Repository\WebsiteRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Created by PhpStorm.
 * User: bartosz.skuza
 * Date: 2017-11-02
 * Time: 17:00
 */
class WebsiteService
{

    /**
     * @var WebsiteRepository
     */
    protected $repository;

    /**
     * WebsiteService constructor.
     * @param WebsiteRepository $repository
     */
    public function __construct(WebsiteRepository $repository)
    {
        $this->repository = $repository;
    }


    public function getAvailable(User $user)
    {
        if (in_array(User::$ROLE_ADMIN, $user->getRoles())) {
            return $this->repository->findAll();
        }
        return $this->repository->getAvailable($user);
    }
}