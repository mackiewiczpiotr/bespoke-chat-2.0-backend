<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 16.01.18
 * Time: 20:48
 */

namespace AppBundle\Service;


use GuzzleHttp\Client;

class LiveChatService
{
    const BASE_URL = 'https://api.livechatinc.com';
    private $login;
    private $password;

    /**
     * @var Client
     */
    private $client;

    /**
     * LiveChatService constructor.
     * @param $login
     * @param $password
     */
    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
        $this->client = new Client(['base_uri' => self::BASE_URL]);
    }

    public function get($uri, $livechatGroup, $params = [])
    {
        $options = $this->getBasicOptions();
        $options['query'] = $params;
        $options['query']['group'] = $livechatGroup;
        $response = $this->client->get($uri, $options);
        return \GuzzleHttp\json_decode($response->getBody()->getContents());
    }

    private function getBasicOptions()
    {
        return [
            'auth' => [$this->login, $this->password],
            'headers' => [
                'X-API-Version' => '2'
            ]
        ];
    }
}