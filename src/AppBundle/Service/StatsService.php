<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 18.01.18
 * Time: 19:57
 */

namespace AppBundle\Service;


use AppBundle\Entity\Website;
use AppBundle\Repository\ChatReportTypeRepository;
use AppBundle\Repository\ChatRepository;

class StatsService
{
    /**
     * @var LiveChatService
     */
    private $livechat;

    /**
     * @var ChartDataParser
     */
    private $parser;

    /**
     * @var ChatReportTypeRepository
     */
    private $chatReportTypeRepository;

    /**
     * @var ChatRepository
     */
    private $chatRepository;

    /**
     * StatsService constructor.
     * @param LiveChatService $livechat
     * @param ChartDataParser $parser
     */
    public function __construct(LiveChatService $livechat, ChartDataParser $parser,
                                ChatReportTypeRepository $chatReportTypeRepository,
                                ChatRepository $chatRepository)
    {
        $this->livechat = $livechat;
        $this->parser = $parser;
        $this->chatReportTypeRepository = $chatReportTypeRepository;
        $this->chatRepository = $chatRepository;
    }

    public function getTotal(Website $website, $from = '', $to = '', $groupByHour = false)
    {
        $params = $this->getRangeParams($from, $to);
        if ($groupByHour) {
            $params['group_by'] = 'hour';
        }
        return $this->parser->parseTotalChatsReport(
            $this->livechat->get('reports/chats/total_chats', $website->getLivechatGroup(), $params));
    }

    public function getAvailability(Website $website, $from = '', $to = '')
    {
        $params = $this->getRangeParams($from, $to);
        return $this->parser->parseAvailability(
            $this->livechat->get('reports/availability', $website->getLivechatGroup(), $params));
    }

    public function getEngagement(Website $website, $from = '', $to = '')
    {
        $params = $this->getRangeParams($from, $to);
        return $this->parser->parseEngagementReport(
            $this->livechat->get('reports/chats/engagement', $website->getLivechatGroup(), $params)
        );
    }

    public function getRatings(Website $website, $from = '', $to = '')
    {
        $params = $this->getRangeParams($from, $to);
        return $this->parser->parseRatingsReport(
            $this->livechat->get('reports/chats/ratings', $website->getLivechatGroup(), $params)
        );
    }

    public function getGreetings(Website $website, $from = '', $to = '')
    {
        $params = $this->getRangeParams($from, $to);
        return $this->parser->parseGreetingsReport(
            $this->livechat->get('reports/chats/greetings', $website->getLivechatGroup(), $params));
    }

    public function getReportType(Website $website, $from = '', $to = '')
    {
        return $this->parser->parseReportTypesInRange(
            $this->chatReportTypeRepository->getRaportTypesInRange($website->getId(), $from, $to)
        );
    }

    public function getReportTypeDaily(Website $website, $from = '', $to = '')
    {
        $reports = $this->chatReportTypeRepository->getReportTypesInRangeDaily($website->getId(), $from, $to);
        $goals = $this->chatRepository->getNumberOfGoalsInRangeDaily($website->getId(), $from, $to);
        return $this->parser->parseReportTypesAndGoalsInRangeDaily(['reports'=>$reports, 'goals'=>$goals],$from,$to);
    }

    private function getRangeParams($from, $to)
    {
        $params = [];
        if (strlen($from) > 0) {
            $params['date_from'] = $from;
        }
        if (strlen($to) > 0) {
            $params['date_to'] = $to;
        }
        return $params;
    }
}