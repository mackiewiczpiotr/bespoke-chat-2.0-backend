<?php
namespace AppBundle\Service;


use AppBundle\Repository\ChatReportRepository;
use AppBundle\DTO\FileExportDetails;
use AppBundle\Entity\Chat\Report\State;

class ChatReportService
{
    
    /**
     * @var ChatReportRepository
     */
    private $repository;

    /**
     * ChatReportService constructor.
     * @param ChatReportRepository $chatReportRepository
     */
    public function __construct(ChatReportRepository $chatReportRepository)
    {
        $this->repository = $chatReportRepository;
    }

    public function getDataForFileExport(FileExportDetails $fileExportDetails)
    {
        $data = $this->repository->getDataForFileExport($fileExportDetails->website, $fileExportDetails->params);
        $parsedData = [];
        $i=0;
        foreach($data as $row)
        {
            $i++;
            $parsedData[]=[
                '#'=>$i,
                "ID raportu"=>$row->getName(),
                "Nazwa klienta"=>($row->getClientName()!==null) ? $row->getClientName():"",
                "Numer telefonu"=>($row->getClientPhone()!==null) ? $row->getClientPhone():"",
                "Adres e-mail"=>($row->getClientEmail()!==null) ? $row->getClientEmail():"",
                "Utworzony"=>$row->getCreated(),
                "Typ"=>$row->getReportType()->getName(),
                "Status"=>$this->getProperStateValue($row->getLastState()->getState()),
            ];
        }
        return $parsedData;
    }

    private function getProperStateValue($state)
    {
        switch($state)
        {
            case State::READ : return "POTWIERDZONY";
            case State::ACCEPTED : return "ZATWIERDZONY";
            case State::CANCELLATION : return "REKLAMACJA ROZPATRYWANA";
            case State::REJECTED : return "ODRZUCONY";
            default : return "";
        }
    }
}