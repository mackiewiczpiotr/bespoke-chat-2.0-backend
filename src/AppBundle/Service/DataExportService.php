<?php
namespace AppBundle\Service;

use AppBundle\Repository\ChatRepository;
use AppBundle\Repository\ChatReportRepository;
use AppBundle\DTO\FileExportDetails;
use Doctrine\ORM\EntityRepository;

class DataExportService
{
    const TYPE_CHATS = "chats";
    const TYPE_REPORTS = "reports";

    /**
     * @var ChatService
     */
    private $chatService;

    /**
     * @var ChatReportService
     */
    private $chatReportService;

    /**
     * @param ChatService $chatService
     * @param ChatReportService $chatReportService
     */
    public function __construct(
        ChatService $chatService, 
        ChatReportService $chatReportService)
    {
        $this->chatService = $chatService;
        $this->chatReportService = $chatReportService;
    }

    /**
     * @param FileExportDetails $fileExportDetails
     */
    public function getData(FileExportDetails $fileExportDetails)
    {
        switch($fileExportDetails->type)
        {
            case self::TYPE_CHATS : $service = $this->chatService; break;
            case self::TYPE_REPORTS : $service = $this->chatReportService; break;
            default: $service = false;
        }
        return ($service) ? $service->getDataForFileExport($fileExportDetails) : [];
    }
}