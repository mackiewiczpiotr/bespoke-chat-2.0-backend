<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 16.01.18
 * Time: 21:43
 */

namespace AppBundle\Service;


use DateInterval;
use DatePeriod;
use DateTime;

class ChartDataParser
{
    public function parseTotalChatsReport($data)
    {
        $total = 0;
        $chartData = [];
        foreach ($data as $label => $valueObj) {
            $chatNum = isset($valueObj->chats) ? $valueObj->chats : 0;
            $chartData[] = ['label' => $label, 'value' => $chatNum];
            $total += $chatNum;
        }
        return [
            'summary' => $total,
            'chartData' => $chartData
        ];
    }

    public function parseAvailability($data)
    {
        $total = 0;
        $count = 0;
        $chartData = [];
        foreach ($data as $label => $valueObj) {
            $chatNum = isset($valueObj->hours) ? $valueObj->hours : 0;
            $chartData[] = ['label' => $label, 'value' => $chatNum];
            $total += $chatNum;
            $count += 1;
        }
        return [
            'summary' => $count !== 0 ? (int)round(($total * 3600) / $count, 0) : 0,
            'chartData' => $chartData
        ];
    }

    public function parseEngagementReport($data)
    {
        $total = [
            'auto' => 0,
            'manual' => 0,
            'immediate' => 0,
        ];
        $chartData = [];
        foreach ($data as $label => $valueObj) {
            $auto = isset($valueObj->chats_from_auto_invite) ? $valueObj->chats_from_auto_invite : 0;
            $manual = isset($valueObj->chats_from_manual_invite) ? $valueObj->chats_from_manual_invite : 0;
            $immediate = isset($valueObj->chats_from_immediate_start) ? $valueObj->chats_from_immediate_start : 0;

            $chartData[] = [
                'label' => $label,
                'auto' => $auto,
                'manual' => $manual,
                'immediate' => $immediate,
            ];
            $total['auto'] += $auto;
            $total['manual'] += $manual;
            $total['immediate'] += $immediate;
        }

        return [
            'summary' => $total,
            'chartData' => $chartData,
        ];
    }

    public function parseRatingsReport($data)
    {
        $total = [
            'bad' => 0,
            'good' => 0,
        ];
        $chartData = [];
        foreach ($data as $label => $valueObj) {
            $bad = isset($valueObj->bad) ? $valueObj->bad : 0;
            $good = isset($valueObj->good) ? $valueObj->good : 0;
            $chartData[] = ['label' => $label, 'bad' => $bad, 'good' => $good];

            $total['bad'] += $bad;
            $total['good'] += $good;
        }
        return [
            'summary' => $total,
            'chartData' => $chartData,
        ];
    }

    public function parseGreetingsReport($data)
    {
        $total = [
            'displayed' => 0,
            'accepted' => 0,
        ];
        $chartData = [];
        foreach ($data as $label => $valueObj) {
            $displayed = isset($valueObj->displayed) ? $valueObj->displayed : 0;
            $accepted = isset($valueObj->accepted) ? $valueObj->accepted : 0;
            $chartData[] = ['label' => $label, 'displayed' => $displayed, 'accepted' => $accepted];

            $total['displayed'] += $displayed;
            $total['accepted'] += $accepted;
        }
        return [
            'summary' => $total,
            'chartData' => $chartData,
        ];
    }

    public function parseReportTypesInRange($data)
    {
        array_walk($data, function (&$item1) {
            $item1['value'] = (int)$item1['value'];
        });
        return [
            'summary' => [],
            'chartData' => $data,
        ];
    }

    public function parseReportTypesAndGoalsInRangeDaily($data, $from, $to)
    {
        $reports =[];
        foreach($data['reports'] as $report)
        {
            $reports[$report['created']]=$report['reports'];
        }
        $goals = $data['goals'];
        foreach($data['goals'] as $goal)
        {
            $goals[$goal['achieved']]=$goal['goals'];
        }
        $period = new DatePeriod(
            new DateTime($from),
            new DateInterval('P1D'),
            new DateTime($to)
        );
        $parsedData = [];
        foreach ($period as $date) {
            $parsedData[$date->format('Y-m-d')] = [
                'reports' => isset($reports[$date->format('Y-m-d')]) ? intval($reports[$date->format('Y-m-d')]) : 0,
                'goals' => isset($goals[$date->format('Y-m-d')]) ? intval($goals[$date->format('Y-m-d')]) : 0
            ];
        }
        $summary = ['reports'=>0, 'goals'=> 0];
        $chartData=[];
        foreach($parsedData as $key=> $value)
        {
            $chartData[] = ['label'=>$key, 'goals' => $value['goals'], 'reports'=>$value['reports']];
            $summary['reports']+=$value['reports'];
            $summary['goals']+=$value['goals'];
            $summary['total']=max($summary);
        }
        return [
            'summary' => $summary,
            'chartData' => $chartData,
        ];
    }
}