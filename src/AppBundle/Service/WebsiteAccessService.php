<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use AppBundle\Entity\User\WebsiteAccess;
use AppBundle\Repository\WebsiteAccessRepository;
use Doctrine\ORM\EntityManagerInterface;

class WebsiteAccessService
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var WebsiteAccessRepository
     */
    protected $repository;

    /**
     * WebsiteAccessService constructor.
     * @param EntityManagerInterface $entityManager
     * @param WebsiteAccessRepository $repository
     */
    public function __construct(EntityManagerInterface $entityManager, WebsiteAccessRepository $repository)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
    }

    /**
     * @param User $user
     * @param Website $website
     * @param string $type
     * @param boolean $isPrimary
     */
    public function add($user, $website, $type, $isPrimary = false)
    {
        $access = $this->repository->findOneBy(['website'=>$website,'user'=>$user]);
        if($access)
        {
            $access->setType($type)->setPrimary($isPrimary);
        }else{
            $access = new WebsiteAccess($type, $website, $user);
            $access->setPrimary($isPrimary);
        }
        $this->em->persist($access);
        $this->em->flush();
        return $this->repository->getUserAccess($user);
    }

    /**
     * @param integer $id
     * @param User $user
     */
    public function remove($id, $user)
    {
        $access = $this->repository->find($id);
        $this->em->remove($access);
        $this->em->flush();
        return $this->repository->getUserAccess($user);
    }
}