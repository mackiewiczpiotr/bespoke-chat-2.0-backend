<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 25.02.18
 * Time: 14:44
 */

namespace AppBundle\Service;


use AppBundle\Entity\Chat;
use AppBundle\Entity\User;
use AppBundle\Repository\ChatCommentRepository;
use AppBundle\Repository\ChatHistoryRepository;
use AppBundle\Repository\ChatRepository;
use AppBundle\DTO\FileExportDetails;

class ChatService
{
    
    /**
     * @var ChatRepository
     */
    private $repository;

    /**
     * @var ChatCommentRepository
     */
    private $commentRepository;

    /**
     * @var ChatHistoryRepository
     */
    private $historyRepository;

    /**
     * ChatService constructor.
     * @param ChatRepository $chatRepository
     * @param ChatCommentRepository $commentRepository
     * @param ChatHistoryRepository $historyRepository
     */
    public function __construct(ChatRepository $chatRepository, ChatCommentRepository $commentRepository, ChatHistoryRepository $historyRepository)
    {
        $this->repository = $chatRepository;
        $this->commentRepository = $commentRepository;
        $this->historyRepository = $historyRepository;
    }


    public function getChatHistory(Chat $chat, User $user)
    {
        if (in_array(User::$ROLE_PARTNER, $user->getRoles())) {
            return $this->historyRepository->getPartnerHistory($chat->getId());
        }
        return $this->historyRepository->getFullHistory($chat->getId());
    }

    public function getChatComments(Chat $chat)
    {
        return $this->commentRepository->getGeneralComments($chat->getId());
    }

    public function getDataForFileExport(FileExportDetails $fileExportDetails)
    {
        $data = $this->repository->getDataForFileExport($fileExportDetails->website, $fileExportDetails->params);
        $parsedData = [];
        $i=0;
        foreach($data as $row)
        {
            $i++;
            $parsedData[]=[
                '#'=>$i,
                "ID czatu"=>$row->getLivechatId(),
                "Rozpoczęcie"=>$row->getStartTime(),
                "Czas trwania"=>gmdate('H:i:s', $row->getSpentTime()),
                "Cele"=>$this->getCollectionNamesToString($row->getReports(), 'name'),
                "Etykiety"=>$this->getCollectionNamesToString($row->getTags(), 'label')
            ];
        }
        return $parsedData;
    }

    private function getCollectionNamesToString($collection, $fieldName)
    {
        $str="";
        foreach($collection as $obj)
            $str.=$obj->{'get' . ucfirst($fieldName)}() . ", ";
        return trim(trim($str),",");
    }
}