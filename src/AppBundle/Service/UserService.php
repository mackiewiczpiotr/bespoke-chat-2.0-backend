<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Entity\User\WebsiteAccess;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\WebsiteAccessRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserService
{
    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var WebsiteAccessRepository
     */
    protected $websiteAccessRepository;

    /**
     * UserService constructor.
     * @param UserPasswordEncoder $passwordEncoder
     * @param WebsiteAccessRepository $websiteAccessRepository
     * @param EntityManager $em
     */
    public function __construct(UserPasswordEncoder $passwordEncoder,
                                WebsiteAccessRepository $websiteAccessRepository,
                                EntityManager $em)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->websiteAccessRepository = $websiteAccessRepository;
        $this->em = $em;
    }

    /**
     * @param User $user
     * @param string $password
     * @return string
     */
    public function generatePassword(User $user, $password)
    {
        return $this->passwordEncoder->encodePassword($user, $password);
    }

    /**
     * @param User $user
     * @param string $oldPassword
     * @return User
     */
    public function updatePassword(User $user, $oldPassword)
    {
        $password = (!empty($user->getPassword())) ? $this->generatePassword($user, $user->getPassword()) : $oldPassword;
        $user->setPassword($password);
        return $user;
    }

    /**
     * @param User $user
     * @return \AppBundle\Entity\Website
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getWorkingContext(User $user)
    {
        $context = $user->getLastWorkingContext();
        if(is_null($context))
        {
            $websiteAccess = $this->websiteAccessRepository->getFirstAvailableAccess($user);
            if(!is_null($websiteAccess)){
                $context = $websiteAccess->getWebsite();
                $user->setLastWorkingContext($context);
                $this->em->persist($user);
                $this->em->flush();

            }
        }
        return $context;
    }

}
