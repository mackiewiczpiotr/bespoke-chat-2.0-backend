<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 27.05.18
 * Time: 19:03
 */

namespace AppBundle\Service;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

class DateRangeCalculator
{

    public function calculate(string $fieldName, QueryBuilder $qb){
        $qb->setMaxResults(1)->setFirstResult(0);
        $qb->resetDQLParts(['select','groupBy']);
        $qb->addSelect('DATE('.$qb->expr()->max($fieldName).') AS maxDate')
            ->addSelect('DATE('.$qb->expr()->min($fieldName).') AS minDate');

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
            return [];
        }
    }
}