<?php

namespace AppBundle\DTO;


use AppBundle\Entity\Website;
use Symfony\Component\HttpFoundation\ParameterBag;
use JMS\Serializer\Annotation\Type;

class FileExportDetails
{
    /**
     * @var Website
     */
    public $website;
    /**
     * @var array
     */
    public $params;
    /**
     * @var string
     */
    public $type;
}