<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 14.01.18
 * Time: 19:23
 */

namespace AppBundle\DTO;


use AppBundle\Entity\Website;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation\Type;

class WebsiteDetails
{
    /**
     * @var Website
     */
    public $details;
    /**
     * @var Collection
     */
    public $agents;

    /**
     * @var Collection
     */
    public $trainers;
    /**
     * @var Collection
     */
    public $admins;

    public $totalChats;

    public $chartData;


}