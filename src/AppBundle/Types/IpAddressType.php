<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 08.02.18
 * Time: 20:02
 */

namespace AppBundle\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class IpAddressType extends Type
{
    const IPADDRESS = 'ipaddress';

    public function getName()
    {
        return self::IPADDRESS;
    }

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $plataform)
    {
        $fieldDeclaration['unsigned'] = true;
        return $plataform->getIntegerTypeDeclarationSQL($fieldDeclaration);
    }

    public function canRequireSQLConversion()
    {
        return true;
    }

    public function convertToPHPValueSQL($sqlExpr, $platform)
    {
        return sprintf('INET_NTOA(%s)', $sqlExpr);
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {

        return sprintf('INET_ATON(%s)', $sqlExpr);
    }


}