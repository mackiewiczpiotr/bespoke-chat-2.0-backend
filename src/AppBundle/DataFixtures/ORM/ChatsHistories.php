<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 18.02.18
 * Time: 17:43
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\Comment;
use AppBundle\Entity\Chat\Comment\Detailed;
use AppBundle\Entity\User;
use AppBundle\Entity\User\WebsiteAccess;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ChatsHistories extends Fixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $start = microtime(true);
        $faker = Factory::create('pl_PL');
        $batchSize = 20;
        $repo = $manager->getRepository(Chat::class);
        $userRepository = $manager->getRepository(User::class);
        $qb = $repo->createQueryBuilder('c');
        $qb->select($qb->expr()->count('c'));
        $total = (int)$qb->getQuery()->getSingleScalarResult();
        $pages = ceil($total / $batchSize);
        var_dump($total);
        var_dump($pages);
        $detailsTypes = [Detailed::CITATION, Detailed::INFORMATION, Detailed::KNOWLEDGE, Detailed::PRAISE, Detailed::SELL, Detailed::TECHNICAL];
        for($i=1; $i<=$pages;$i++) {
            $chats = $repo->findBy([], ['id'=>'ASC'], $batchSize, $batchSize * ($i - 1));

            foreach ($chats as $chat) {
                $agents = $chat->getAgents()->toArray();
                $commentsNum = $faker->numberBetween(0, 5);
                for ($j = 0; $j < $commentsNum; $j++) {

                    $owner = $userRepository->getOwners($chat->getWebsite())[0];
                    $clientComment = new Comment(
                        $chat,
                        $owner,
                        $faker->text);
                    $chat->addComment($clientComment);
                }
                $historyNum = $faker->numberBetween(10, 20);

                for ($j = 0; $j < $historyNum; $j++) {
                    $history = null;
                    if ($faker->boolean) {
                        $agent = null;
                        if ($faker->boolean) {
                            $authorName = $chat->getClientName();
                        } else {
                            $agent = $faker->randomElement($agents);
                            $authorName = $agent->getName();
                        }
                        $history = new Chat\History\Message(
                            $chat,
                            $faker->dateTime,
                            $faker->sentence,
                            $authorName);
                        if(!is_null($agent)){
                            $history->setAgent($agent);
                        }

                        for($k=0;$k<5;$k++){
                            new Detailed(
                                $chat,
                                $faker->randomElement($agents),
                                $history,
                                $faker->randomElement($agents),
                                Detailed::PRAISE,
                                $faker->text);
                        }

                    } else if ($faker->boolean) {
                        $history = new Chat\History\VisitedPage(
                            $chat,
                            $faker->dateTime,
                            $faker->url,
                            $faker->domainName);
                    } else if ($faker->boolean){
                        $history = new Chat\History\AutotrainerTip(
                            $chat,
                            $faker->dateTime,
                            $faker->sentence
                        );
                        if($faker->boolean){
                            if($faker->boolean){
                                $history->approve($faker->randomElement($agents));
                            } else{
                                $history->decline($faker->randomElement($agents));
                            }
                        }
                    }
                    else if($faker->boolean){
                        $history = new Chat\History\Event($chat,
                            $faker->dateTime,
                            $faker->sentence);
                    }
                    else {
                        $history = new Chat\History\TrainerTip($chat,
                            $faker->dateTime,
                            $faker->randomElement($agents),
                            $faker->sentence);
                    }
                    if($faker->boolean){

                        for($k=0;$k<5;$k++){
                            new Detailed(
                                $chat,
                                $faker->randomElement($agents),
                                $history,
                                $faker->randomElement($agents),
                                $faker->randomElement($detailsTypes),
                                $faker->text);
                        }
                    }
                    $chat->addHistory($history);
                }
                $manager->persist($chat);
            }
            $manager->flush();
            $manager->clear();
        }
        $time_elapsed_secs = microtime(true) - $start;
        var_dump('ChatsHistories: ' . $time_elapsed_secs);
    }


    public function getDependencies()
    {
        return array(
            Chats::class,
        );
    }
}