<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Chat\Report\Type;
use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use AppBundle\Entity\User\WebsiteAccess;
use AppBundle\Entity\Website;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class Basic extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $start = microtime(true);
        $encoder = $this->container->get('security.password_encoder');

        $admin = new User('admin', 'admin@bespoke.pl');
        $admin->setPassword($encoder->encodePassword($admin, 'admin'));
        $admin->setRoles([User::$ROLE_ADMIN]);
        $manager->persist($admin);


        $faker = Factory::create('pl_PL');
        $partnersNumber = $faker->numberBetween(10, 15);

        $teams = [
            new Team('A'),
            new Team('B'),
            new Team('C'),
        ];
        $teamsNumber = count($teams);
        foreach ($teams as $team) {
            $manager->persist($team);
        }

        $agentsNumber = $faker->numberBetween(20, 40);
        $createdAgents = [];
        for ($i = 0; $i < $agentsNumber; $i++) {
            $agent = new User($faker->unique()->userName, $faker->email);
            $agent->setPassword($encoder->encodePassword($agent, 'agent'));
            $agent->setRoles([User::$ROLE_AGENT]);
            $agent->setFirstName($faker->firstName);
            $agent->setLastName($faker->lastName);
            $gender = $faker->boolean ? 'men' : 'women';
            $avatarNumber = $faker->numberBetween(1, 96);
            $agent->setAvatar("https://randomuser.me/api/portraits/$gender/$avatarNumber.jpg");
            $agent->setPhone($faker->phoneNumber);
            $manager->persist($agent);
            $createdAgents[] = $agent;
            $teams[$faker->numberBetween(0, $teamsNumber - 1)]->addMember($agent);
        }

        $reportTypes = [
            'ZWROTY',
            'Zgłoszenie firmy',
            'ZGLOSZENIE-PROMOCJA KONSULTACJE',
            'Zapytanie p. Sylwia',
            'Zapytanie p. Sebastian',
            'Zapytanie p. Patrycja',
            'Zapytanie p. Mateusz',
            'Zapytanie p. Karolina',
            'Zapytanie p. Kacper',
            'zapytanie ofertowe',
            'ZAPYTANIE O OFERTĘ PRACY',
            'ZAMÓWIENIE NA 3',
            'ZAMÓWIENIE NA 2',
            'ZAMOWIENIE',
            'Wynajem auta',
            'wymiary odziezy',
            'Wroclaw Teczowa',
            'Wroclaw Stablowicka',
            'Wroclaw Soltysowicka',
            'Wroclaw Osiedle Potokowa',
            'Wroclaw Osiedle Nowalia',
            'Wroclaw Osiedle Maestro',
            'Wroclaw Osiedle Graniczna',
            'Wroclaw Miedzy Parkami',
            'Wroclaw Kamienna 145',
            'Wroclaw Kamienna',
            'Wroclaw Dolina Piastow',
            'Wroclaw Brzeska',
            'Wroclaw - SKP',
            'Wroclaw - Serwis blacharsko lakierniczy',
            'Wroclaw - Serwis - informacja',
            'Wroclaw - Serwis',
            'Wroclaw - Samochody uzywane',
            'Wroclaw - Salon - informacja',
            'Wroclaw - Salon',
            'Wroclaw - Czesci i akcesoria ',
            'WIZYTA KOMERCYJNA',
            'Warszawa Rezydencja Kaliska',
            'Warszawa Powstancow 33',
            'Warszawa Poborzanska',
            'Warszawa Osiedle Przy Promenadzie',
            'Warszawa Osiedle Na Woli II',
            'Warszawa Osiedle Na Woli I',
            'Warszawa Mala Praga',
            'Warszawa Krzemowe',
            'Warszawa Korona Pragi',
            'UZYWANE OLSZTYN',
            'UZYWANE MODLINSKA',
            'UZYWANE JEROZOLIMSKIE',
            'UZYWANE',
            'subskrypcja',
            'STACJA KONTROLI POJAZDÓW',
            'SPRZEDAZ FLOTOWA',
            'SERWIS MODLINSKA- SKODA',
            'SERWIS MODLINSKA - Audi, VW',
            'SERWIS JEROZOLIMSKIE- SKODA',
            'SERWIS JEROZOLIMSKIE - Audi, VW',
            'serwis',
            'SALON MODLINSKA- SKODA',
            'SALON MODLINSKA - Audi, VW',
            'SALON JEROZOLIMSKIE- SKODA',
            'SALON JEROZOLIMSKIE - Audi, VW',
            'SALON',
            'REZERWACJA',
            'Reklamacje wypowiedzenia',
            'reklamacja',
            'PYTANIE TECHNICZNE',
            'pytanie',
            'PYTANIA KSIĘGOWE',
            'PROGRAM PARTNERSKI ',
            'Problemy Techniczne',
            'Problem Techniczny',
            'POTENCJALNY KLIENT',
            'PLATNOSCI',
            'OPIS PRODUKTU',
            'OBECNY KLIENT',
            'Nowy klient',
            'Mieszkania',
            'Lubin - SKP',
            'Lubin - Serwis',
            'Lubin - Samochody uzywane',
            'Lubin - Salon',
            'Lubin - Informacja',
            'Lubin - Czesci i akcesoria ',
            'Lokale usługowe',
            'Lodz Pustynna 43',
            'Lodz Debowa Ostoja',
            'LEAD-Złota Podkowa',
            'LEAD-Zielone Wzgorze',
            'LEAD-Wybrzeze Reymonta',
            'LEAD-Suwalska',
            'LEAD-Sloneczny Oltaszyn',
            'LEAD-Serwis',
            'LEAD-Samochody używane',
            'LEAD-Salon',
            'LEAD-Ritmo Park',
            'LEAD-Redzinska',
            'LEAD-raport testowy',
            'LEAD-PROGRAM PARTNERSKI',
            'LEAD-Primera',
            'LEAD-Partynice House',
            'LEAD-Osiedle w Alejach',
            'LEAD-Osiedle Sudeckie',
            'LEAD-Osiedle Familia',
            'LEAD-Oltaszyn!',
            'LEAD-Nowe Iwiny',
            'LEAD-Nova Sołtysowicka 29',
            'LEAD-MPoint',
            'LEAD-Kukuczki 7',
            'LEAD-Komercyjne',
            'LEAD-Kepa Mieszczanska',
            'LEAD-inne',
            'LEAD-Czesci akcesoria',
            'LEAD-12 Miesięcy',
            'lead',
            'LC Corp - test',
            'Krakow Wrocławska',
            'Krakow Sloneczne Miasteczko',
            'Krakow Przy Mogilskiej',
            'Krakow Okulickiego',
            'Krakow Grzegorzecka',
            'Krakow Centralna Park',
            'Krakow - SKP',
            'Krakow - Serwis',
            'Krakow - Samochody uzywane',
            'Krakow - Salon',
            'Krakow - informacja',
            'Krakow - Czesci i akcesoria',
            'KONTAKT Z KURIEREM ',
            'jazda testowa',
            'Informacja-TK/USG/RTG',
            'INFORMACJA-SERWIS',
            'INFORMACJA-Samochody używane',
            'INFORMACJA-SALON',
            'Informacja-Rezonans',
            'INFORMACJA-Czesci akcesoria',
            'informacja',
            'Gdansk Swietokrzyska Park',
            'Gdansk Przy Alejach',
            'Gdansk Osiedle Przy Srebrnej',
            'Gdansk Bastion Wałowa',
            'Gdansk Bajkowy Park',
            'FLOTA',
            'CZĘŚCI I AKCESORIA',
            'CZESCI AKCESORIA MODLINSKA- SKODA',
            'CZESCI AKCESORIA MODLINSKA',
            'CZESCI AKCESORIA JEROZOLIMSKIE- SKODA',
            'CZESCI AKCESORIA JEROZOLIMSKIE - Audi, VW',
            'CECHY PRODUKTU',
            'błąd w ogłoszeniu',
            'BYŁY PRACOWNIK',
            'BLEDNY OPIS',
            'BETA-PROBLEM TECHNICZNY',
            'BETA-INFORMACJA',
            'BADANIE KOMERCYJNE-TK/USG/RTG',
            'BADANIE KOMERCYJNE-Rezonans',
            'API FAILED',
            'PACZKA ZWROTNA'
        ];


        for ($i = 0; $i < $partnersNumber; $i++) {
            $partner = new User($faker->unique()->userName, $faker->email);
            $partner->setPassword($encoder->encodePassword($partner, 'partner'));
            $partner->setRoles([User::$ROLE_PARTNER]);
            $partner->setFirstName($faker->firstName);
            $partner->setLastName($faker->lastName);
            $manager->persist($partner);

            $companyNumber = $faker->randomDigitNotNull;
            for ($j = 0; $j < $companyNumber; $j++) {
                $website = new Website(
                    $faker->company,
                    $faker->randomElement($teams),
                    substr($faker->md5, 0, 32),
                    substr($faker->md5, 0, 32)
                );

                $manager->persist(new WebsiteAccess(WebsiteAccess::OWNER, $website, $partner));
                $kam = $faker->randomElement($createdAgents);
                $kam->setContactEmail($faker->companyEmail);
                $kam->setContactPhone($faker->phoneNumber);

                $trainer = $faker->randomElement($createdAgents);
                $trainer->setContactEmail($faker->companyEmail);
                $trainer->setContactPhone($faker->phoneNumber);
                $manager->persist((new WebsiteAccess(WebsiteAccess::KAM, $website, $kam))
                    ->setPrimary(true));
                $manager->persist((new WebsiteAccess(WebsiteAccess::TRAINER, $website, $trainer))
                    ->setPrimary(true));

                if ($faker->boolean) {
                    $website->setVatEmail($faker->companyEmail);
                }
                $agents = $faker->randomElements($createdAgents,$faker->randomDigitNotNull);

                foreach($agents as $agent){
                    $manager->persist(new WebsiteAccess(WebsiteAccess::AGENT, $website, $agent));
                }

                $manager->persist($website);

                $reportTypesForWebsite = $faker->randomElements($reportTypes, $faker->randomDigitNotNull);
                foreach ($reportTypesForWebsite as $typeName) {
                    $reportType = new Type($typeName, $faker->sentence, $website);
                    $website->addChatReportType($reportType);

                    $notificationsNum = $faker->randomDigitNotNull;

                    for ($k = 0; $k < $notificationsNum; $k++) {
                        $notification = null;
                        if ($faker->boolean) {
                            $notification = Type\Notification::createEmailNotification($reportType, $faker->companyEmail);
                        } else {
                            $notification = Type\Notification::createSmsNotification($reportType, $faker->phoneNumber);
                        }
                        $reportType->addNotification($notification);
                    }
                }
            }
        }

        $manager->flush();
        $manager->clear();
        $time_elapsed_secs = microtime(true) - $start;
        var_dump('Basic: ' . $time_elapsed_secs);
    }
}