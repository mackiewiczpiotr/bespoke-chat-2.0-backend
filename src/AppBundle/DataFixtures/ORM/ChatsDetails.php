<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 18.02.18
 * Time: 17:43
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\Goal;
use AppBundle\Entity\Chat\Survey;
use AppBundle\Entity\Chat\Variable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ChatsDetails extends Fixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $start = microtime(true);
        $faker = Factory::create('pl_PL');
        $batchSize = 20;
        $repo = $manager->getRepository(Chat::class);
        $qb = $repo->createQueryBuilder('c');
        $qb->select($qb->expr()->count('c'));
        $total = (int)$qb->getQuery()->getSingleScalarResult();
        $pages = ceil($total / $batchSize);

        for ($i = 1; $i <= $pages; $i++) {
            $chats = $repo->findBy([], ['id' => 'ASC'], $batchSize, $batchSize * ($i - 1));
            foreach ($chats as $chat) {
                $surveysNum = $faker->numberBetween(10, 20);

                for ($j = 0; $j < $surveysNum; $j++) {
                    if ($faker->boolean) {
                        $chat->addSurvey(
                            Survey::createPreSurvey($chat, $faker->text, $faker->text)
                        );
                    } else {
                        $chat->addSurvey(
                            Survey::createPostSurvey($chat, $faker->text, $faker->text)
                        );
                    }
                }
                $goalsNum = $faker->numberBetween(10,20);

                for($j=0;$j<$goalsNum;$j++) {
                    $chat->addGoal(
                        new Goal(
                            $chat,
                            substr($faker->md5,0,25),
                            $faker->dateTime,
                            $faker->word
                        )
                    );
                }
                $variablesNum = $faker->numberBetween(10,20);
                for($j=0;$j<$variablesNum;$j++) {
                    $chat->addVariable(
                        new Variable($chat, $faker->word, $faker->word)
                    );
                }



                $manager->persist($chat);
            }
            $manager->flush();
            $manager->clear();
        }
        $time_elapsed_secs = microtime(true) - $start;
        var_dump('ChatsDetails: ' . $time_elapsed_secs);
    }


    public function getDependencies()
    {
        return array(
            ChatsHistories::class,
        );
    }
}