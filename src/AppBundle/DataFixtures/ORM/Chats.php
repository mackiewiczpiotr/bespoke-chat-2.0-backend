<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 18.02.18
 * Time: 17:43
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\Report;
use AppBundle\Entity\Chat\Report\State;
use AppBundle\Entity\Tag;
use AppBundle\Entity\User;
use AppBundle\Entity\User\WebsiteAccess;
use AppBundle\Entity\Website;
use AppBundle\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class Chats extends Fixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $start = microtime(true);
        $faker = Factory::create('pl_PL');
        $availableRatingScore = ['rated', 'not_rated', 'rated_good', 'rated_bad'];
        $websites = $manager->getRepository(Website::class)->findAll();
        $userRepostiory = $manager->getRepository( User::class);
        $tags = [];
        $tagsLabels = ['Poszukiwanie',
            'Zwykła rozmowa',
            'Subskrypcja',
            'Spam',
            'Serwis',
            'Lead',
            'Sprzedaż',
            'Zamówienie',
            'Jazda testowa',
            'Problem Techniczny',
            'Nadane pranie',
            'Rekrutacja',
            'Wpis',
            'Pytanie',
            'Rejestracja',
            'Obsługa klienta',
            'Zapytanie ofertowe'];
        foreach ($tagsLabels as $label) {
            $tag = new Tag($label);
            $tags[] = $tag;
            $manager->persist($tag);
        }

        foreach ($websites as $website) {
            $chatNumber = $faker->numberBetween(5, 40);
            $operators = $userRepostiory->getAgents($website);
            for ($i = 0; $i < $chatNumber; $i++) {
                $agents = $faker->randomElements($operators, $faker->numberBetween(1, count($operators)));

                $chat = new Chat($website,
                    $faker->md5,
                    $faker->firstName . ' ' . $faker->lastName,
                    $faker->dateTimeBetween('-1 year'),
                    new ArrayCollection($agents));

                $chat->setClientCity($faker->city);
                $chat->setClientCountry('Poland');
                $chat->setClientRefferer($faker->url);
                $chat->setStartedSite($faker->url);
                $chat->setClientIp($faker->ipv4);
                $chat->setClientUserAgent($faker->userAgent);
                $chat->setClientId(substr($faker->md5, 0, 25));
                if ($faker->boolean) {
                    $chat->setRate($faker->randomElement($availableRatingScore));
                }

                $tagsToAdd = $faker->randomElements($tags, $faker->numberBetween(0, count($tags)));
                foreach ($tagsToAdd as $tag) {
                    $chat->addTag($tag);
                }
                if ($faker->boolean) {
                    $endTime = $faker->dateTimeBetween($chat->getStartTime()->format('Y-m-d H:i:s'), '+1 day');
                    $chat->setEndTime($endTime);
                    $chat->setSpentTime($endTime->getTimestamp() - $chat->getStartTime()->getTimestamp());
                }
                if ($faker->boolean(70)) {
                    $reportType = $faker->randomElement($website->getChatReportTypes()->toArray());
                    $report = new Report(substr($faker->md5, 0, 12), $faker->dateTimeBetween('-1 year'), $chat, $faker->randomElement($agents), $reportType);
                    $report->setDescription($faker->paragraph);
                    $report->setClientEmail($faker->email);
                    $report->setClientName($faker->name);
                    $report->setClientPhone($faker->phoneNumber);


                    if ($faker->boolean) {
                        $readState = State::createReadStatus($faker->dateTimeBetween('-1 year'), $report);
                        if ($faker->boolean) {
                            $readState->setCreatedBy($faker->randomElement($agents));
                        }
                        $manager->persist($readState);
                        $report->addState($readState);

                        if ($faker->boolean) {
                            $acceptedState = State::createAcceptedStatus($faker->dateTimeBetween('-1 year'), $report);
                            if ($faker->boolean) {
                                $acceptedState->setCreatedBy($faker->randomElement($agents));
                            }
                            $manager->persist($acceptedState);
                            $report->addState($acceptedState);

                        } elseif ($faker->boolean) {
                            $cancellationState = State::createCancellationStatus($faker->dateTimeBetween('-1 year'), $report);
                            if ($faker->boolean) {
                                $cancellationState->setCreatedBy($faker->randomElement($agents));
                            }
                            $manager->persist($cancellationState);
                            $report->addState($cancellationState);

                        } elseif ($faker->boolean) {
                            $rejectedState = State::createRejectedStatus($faker->dateTimeBetween('-1 year'), $report);
                            if ($faker->boolean) {
                                $rejectedState->setCreatedBy($faker->randomElement($agents));
                            }
                            $manager->persist($rejectedState);
                            $report->addState($rejectedState);

                        }
                    }
                    $chat->addReport($report);
                }
                $manager->persist($chat);
            }
        }

        $manager->flush();
        $manager->clear();
        $time_elapsed_secs = microtime(true) - $start;
        var_dump('Chats: ' . $time_elapsed_secs);
    }

    public function getDependencies()
    {
        return array(
            Basic::class,
        );
    }
}