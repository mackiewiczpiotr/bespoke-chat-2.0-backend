<?php
/**
 * Created by PhpStorm.
 * User: bartoszskuza
 * Date: 02/06/2018
 * Time: 18:04
 */

namespace AppBundle\Security;


use AppBundle\Entity\User;
use AppBundle\Exception\AccountDisabledException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{

    /**
     * Checks the user account before authentication.
     *
     * @param UserInterface $user a UserInterface instance
     *
     * @throws AccountStatusException
     */
    public function checkPreAuth(UserInterface $user)
    {
        if(!$user instanceof User){
            return;
        }

        if(!$user->isActive()){
            throw new AccountDisabledException('Konto nie jest aktywne');
        }
    }

    /**
     * Checks the user account after authentication.
     *
     * @param UserInterface $user a UserInterface instance
     *
     * @throws AccountStatusException
     */
    public function checkPostAuth(UserInterface $user)
    {

    }
}