<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 10.12.17
 * Time: 23:16
 */

namespace AppBundle\Security;

use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use AppBundle\Repository\WebsiteAccessRepository;
use AppBundle\Repository\WebsiteRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class WebsiteVoter extends Voter
{
    const ACCESS = 'access';

    private $decisionManager;

    private $repository;

    public function __construct(AccessDecisionManagerInterface $decisionManager, WebsiteAccessRepository $repository)
    {
        $this->decisionManager = $decisionManager;
        $this->repository = $repository;
    }

    protected function supports($attribute, $subject)
    {

        if ($attribute !== self::ACCESS) {
            return false;
        }
        if (!($subject instanceof Website)) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if ($this->decisionManager->decide($token, [User::$ROLE_ADMIN])) {
            return true;
        }
        return $this->repository->hasAccess($user,$subject);
    }

}