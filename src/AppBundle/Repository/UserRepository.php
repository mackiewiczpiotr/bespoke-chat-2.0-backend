<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 04.01.18
 * Time: 16:49
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User\WebsiteAccess;
use AppBundle\Entity\Website;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class UserRepository extends EntityRepository
{
    private $excludedAgents = [];

    public function getWorkingWithWebsite($id){
        $rootQuery = "SELECT u.*
                FROM websites w 
                JOIN chats c ON w.id=c.website
                JOIN chats_agents ca ON c.id = ca.chat
                JOIN users u ON ca.agent=u.id
                WHERE w.id = :websiteId AND u.is_active=1 ";

        $groupBy = ' GROUP BY u.id';

        $excludedAgentsParts = '';

        for($i = 0;$i<count($this->excludedAgents); $i++){
            $excludedAgentsParts .=' AND u.email !=:excluded'.$i++;
        }

        $rsm = new ResultSetMappingBuilder($this->getEntityManager());

        $query = $rootQuery.$excludedAgentsParts.$groupBy;
        $rsm->addRootEntityFromClassMetadata('AppBundle\Entity\User', 'u');
        $query = $this->getEntityManager()->createNativeQuery($query,$rsm);

        $i = 0;
        foreach ($this->excludedAgents as $agentEmail){
            $query->setParameter('excluded'.$i++,$agentEmail,'string');
        }

        return $query->setParameter('websiteId',$id)->getResult();
    }
    
    public function getWebsiteTeam(Website $website){
        $qb = $this->getByTypesQuery($website, [WebsiteAccess::TRAINER, WebsiteAccess::AGENT]);
        return $qb->getQuery()->getResult();
    }

    public function getAgents(Website $website){
        $qb = $this->getByTypesQuery($website, [WebsiteAccess::AGENT]);
        return $qb->getQuery()->getResult();
    }
    
    public function getOwners(Website $website){
        $qb = $this->getByTypesQuery($website, [WebsiteAccess::OWNER]);
        return $qb->getQuery()->getResult();
    }
    
    public function getAdmins(Website $website){
        $qb = $this->getByTypesQuery($website, [WebsiteAccess::KAM]);
        $qb->andWhere($qb->expr()->eq('a.primary',':primary'))
        ->setParameter('primary',true);
        return $qb->getQuery()->getResult();
    }

    public function getTrainers(Website $website){
        $qb = $this->getByTypesQuery($website, [WebsiteAccess::TRAINER]);
        $qb->andWhere($qb->expr()->eq('a.primary',':primary'))
            ->setParameter('primary',true);
        return $qb->getQuery()->getResult();
    }

    public function getFindAllQuery()
    {
        return $this->createQueryBuilder('u');
    }

    
    /**
     * @param array $excludedAgents
     */
    public function setExcludedAgents($excludedAgents)
    {
        $this->excludedAgents = $excludedAgents;
    }



    private function getByTypesQuery(Website $website, $types){
        $qb = $this->createQueryBuilder('u');
        $qb->innerJoin('AppBundle\Entity\User\WebsiteAccess', 'a', 'WITH',
            $qb->expr()->andX(
                    $qb->expr()->eq('u', 'a.user'),
                    $qb->expr()->eq('a.website', ':websiteId'),
                    $qb->expr()->in('a.type',':types')
            ))
            ->andWhere($qb->expr()->eq('u.isActive','1'))
            ->setParameter('types',$types)
            ->setParameter('websiteId', $website->getId());

        return $qb;
    }
}