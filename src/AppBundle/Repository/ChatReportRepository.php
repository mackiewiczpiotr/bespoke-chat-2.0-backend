<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 30.11.17
 * Time: 20:46
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Website;

class ChatReportRepository extends EntityRepository
{
    public function getBelongToWebsiteQuery($websiteId)
    {
        return $this->createQueryBuilder('r')
            ->join('r.reportType','t')
            ->innerJoin('r.chat', 'c')
            ->innerJoin('c.website','w','WITH','w.id =:website')
            ->setParameter('website',$websiteId);
    }

    public function getBelongToChat($chatId){
        $qb = $this->createQueryBuilder('r');
        return $qb->andWhere($qb->expr()->eq('r.chat',':chatId'))
            ->setParameter('chatId',$chatId)->getQuery()->getResult();
    }

    public function getDataForFileExport(Website $website, $params)
    {
        $qb = $this->getBelongToWebsiteQuery($website->getId());
        if(!empty($params['startDate']) && !empty($params['endDate']))
            $qb->andWhere('DATE(r.created) BETWEEN :from AND :to')
                ->setParameter('from', $params['startDate'] )
                ->setParameter('to', $params['endDate']);
        if(!empty($params['sortField']) && !empty($params['sortOrder']))
            $qb->orderBy('r.'.$params['sortField'], $params['sortOrder']);
            
        return $qb->getQuery()->getResult();
    }
}