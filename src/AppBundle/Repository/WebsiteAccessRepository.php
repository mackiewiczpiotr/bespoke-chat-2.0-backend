<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 21.03.18
 * Time: 21:07
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class WebsiteAccessRepository extends EntityRepository
{

    public function hasAccess(User $user, Website $website)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select($qb->expr()->count('a.id'))
            ->andWhere($qb->expr()->eq('a.website', ':websiteId'))
            ->andWhere($qb->expr()->eq('a.user', ':userId'))
            ->setParameter('websiteId', $website->getId())
            ->setParameter('userId', $user->getId());
        try {
            return $qb->getQuery()->getSingleResult() > 0;
        } catch (NoResultException $e) {
            return false;
        } catch (NonUniqueResultException $e) {
            return false;
        }
    }

    public function getUserAccess(User $user)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->join('AppBundle\Entity\Website', 'w', 'WITH', 'w.id=a.website')
            ->andWhere($qb->expr()->eq('a.user', ':user'))
            ->setParameter('user', $user->getId());
        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return User\WebsiteAccess|null
     */
    public function getFirstAvailableAccess(User $user){
        $qb = $this->createQueryBuilder('a');
        $qb->andWhere($qb->expr()->eq('a.user', ':userId'))
            ->setParameter('userId', $user->getId())
            ->setMaxResults(1);
        $query = $qb->getQuery();
        try {
            return $query->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
    
}