<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 30.11.17
 * Time: 20:46
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use AppBundle\Entity\Chat\Comment\Detailed;

class ChatCommentRepository extends EntityRepository
{
    /**
     * @param $websiteId
     * @param int $limit
     * @return array
     */
    public function getPraisesForWebsite($websiteId, $limit = 5)
    {
        $query = "SELECT c.*
            FROM chats_comments c 
            LEFT JOIN chats ON c.chat = chats.id
            WHERE c.comment_type='detailed' 
            AND c.details_type = :detailsType
            AND chats.website = :websiteId
            AND c.chat IS NOT NULL
            ORDER BY RAND() LIMIT :limit";
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata('AppBundle\Entity\Chat\Comment\Detailed', 'c');
        $query = $this->getEntityManager()->createNativeQuery($query,$rsm);
        
        return $query->setParameter('websiteId',$websiteId)
            ->setParameter('detailsType', Detailed::CITATION)
            ->setParameter('limit',$limit)
            ->getResult();
    }

    public function getGeneralComments($chatId)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->andWhere($qb->expr()->eq('c.chat', ':chatId'))
            ->setParameter('chatId', $chatId);
        $qb->andWhere($qb->expr()->not($qb->expr()->isInstanceOf('c', 'AppBundle\Entity\Chat\Comment\Detailed')));
        return $qb->getQuery()->getResult();
    }

}