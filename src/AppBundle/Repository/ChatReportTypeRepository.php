<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 04.02.18
 * Time: 13:26
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ChatReportTypeRepository extends EntityRepository
{
    public function getRaportTypesInRange($websiteId, $from = '', $to = '')
    {
        $qb = $this->getBaseQuery($websiteId,$from,$to);
        $qb->select($qb->expr()->count('t') . ' AS value', 't.name')
            ->groupBy('t.name');
        return $qb->getQuery()->getResult();
    }
    public function getReportTypesInRangeDaily($websiteId, $from = '', $to = ''){
        $qb = $this->getBaseQuery($websiteId,$from,$to);
        $qb->select($qb->expr()->count('t') . ' AS reports', 'DATE(r.created) AS created')
            ->addGroupBy('created');
        return $qb->getQuery()->getResult();
    }

    private function getBaseQuery($websiteId, $from = '', $to = '')
    {
        $qb = $this->createQueryBuilder('t');
        $qb
            ->join('AppBundle\Entity\Chat\Report', 'r', 'WITH', 'r.reportType=t.id')
            ->innerJoin('AppBundle\Entity\Chat', 'c', 'WITH', 'r.chat=c.id')
            ->andWhere($qb->expr()->eq('t.website', ':website'))

            ->setParameter('website', $websiteId);
        if (strlen($from) > 0) {
            $qb->andWhere($qb->expr()->gte('r.created',':from'))
                ->setParameter('from',$from);
        }
        if (strlen($to) > 0) {
            $qb->andWhere($qb->expr()->lte('r.created',':to'))
                ->setParameter('to',$to);
        }
        return $qb;
    }
}