<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 05.11.17
 * Time: 17:02
 */
class WebsiteRepository extends EntityRepository
{
    public function findAll()
    {
        $qb = $this->createQueryBuilder('w');
        $qb->join('w.team','t')
            ->andWhere($qb->expr()->isNull('w.deleted'))
            ->addOrderBy('t.name')
            ->addOrderBy('w.name');
        return $qb->getQuery()->getResult();
    }

    public function getAvailable(User $user)
    {
        $qb = $this->createQueryBuilder('w');
        $qb->innerJoin('AppBundle\Entity\User\WebsiteAccess', 'a', 'WITH',
            $qb->expr()->andX(
                    $qb->expr()->eq('w', 'a.website'),
                    $qb->expr()->eq('a.user', ':userId')
            ))
        ->join('w.team','t')
            ->setParameter('userId', $user->getId())
            ->andWhere($qb->expr()->isNull('w.deleted'))
            ->addOrderBy('t.name')
            ->addOrderBy('w.name');

        return $qb->getQuery()->getResult();
    }

}