<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 30.11.17
 * Time: 20:46
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Chat\Comment\Detailed;
use Doctrine\ORM\EntityRepository;

class ChatHistoryRepository extends EntityRepository
{
    public function getPartnerHistory($chatId)
    {
        $qb = $this->getBaseQuery($chatId);
        $qb->andWhere(
            $qb->expr()->not(
                $qb->expr()->isInstanceOf('h', 'AppBundle\Entity\Chat\History\AutotrainerTip')
            )
        );
        return $qb->getQuery()->getResult();
    }


    public function getFullHistory($chatId)
    {
        $qb = $this->getBaseQuery($chatId);
        return $qb->getQuery()->getResult();
    }

    private function getBaseQuery($chatId)
    {
        $qb = $this->createQueryBuilder('h');
        $qb->andWhere($qb->expr()->eq('h.chat', ':chatId'))
            ->setParameter('chatId', $chatId)
            ->addOrderBy('h.createdAt', 'ASC');
        return $qb;
    }
}