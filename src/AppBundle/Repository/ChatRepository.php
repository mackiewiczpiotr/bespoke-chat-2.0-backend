<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 30.11.17
 * Time: 20:46
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Chat;
use AppBundle\Entity\Website;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\ResultSetMapping;

class ChatRepository extends EntityRepository
{
    public function getBelongToWebsite($id)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->leftJoin('c.tags', 't')
            ->groupBy('c.id');
        return $qb->where($qb->expr()->eq('c.website', $id));
    }

    public function getNumberOfGoalsInRangeDaily($id, $from = '', $to = '')
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select($qb->expr()->count('g') . ' AS goals', 'DATE(g.achieved) AS achieved')
        ->join('c.goals', 'g')
        ->where($qb->expr()->eq('c.website', $id))
        ->addGroupBy('achieved');
        if(!empty($from) && !empty($to))
            $qb->andWhere('g.achieved BETWEEN :from AND :to')
                ->setParameter('from', $from )
                ->setParameter('to', $to);
        return $qb->getQuery()->getResult();
    }

    public function getDataForFileExport(Website $website, $params)
    {
        $qb = $this->getBelongToWebsite($website->getId());
        if(!empty($params['startDate']) && !empty($params['endDate']))
            $qb->andWhere('DATE(c.startTime) BETWEEN :from AND :to')
                ->setParameter('from', $params['startDate'] )
                ->setParameter('to', $params['endDate']);
        if(!empty($params['sortField']) && !empty($params['sortOrder']))
            $qb->orderBy('c.'.$params['sortField'], $params['sortOrder']);
        
        return $qb->getQuery()->getResult();
    }

    public function getTotalNumberOfChatsFromWeek(Website $w)
    {
        $start = (new DateTime())->sub(new DateInterval('P7D'));
        $end = new DateTime();

        $sql = 'SELECT IFNULL(SUM(chats.total),0) AS total '.$this->getBaseQueryChatsByDaysFromWeek();

        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('total', 'value', 'integer');
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $query->setParameter('start', $start->format('Y-m-d'))
            ->setParameter('end', $end->format('Y-m-d'))
            ->setParameter('website', $w->getId());

        try {
            return (int)$query->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

    public function getNumberOfChatsGroupedByDaysFromWeek(Website $w)
    {
        $start = (new DateTime())->sub(new DateInterval('P7D'));
        $end = new DateTime();

        $sql = 'SELECT IFNULL(chats.total,0) AS total, gen_date as started_date '.$this->getBaseQueryChatsByDaysFromWeek();


        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('total', 'value', 'integer');
        $rsm->addScalarResult('started_date', 'date');
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $query->setParameter('start', $start->format('Y-m-d'))
            ->setParameter('end', $end->format('Y-m-d'))
            ->setParameter('website', $w->getId());

        return $query->getResult();

    }

    public function getChatsWithTheSameClient(Chat $chat, $limit = 20)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->andWhere($qb->expr()->neq('c.id', $chat->getId()))
            ->andWhere($qb->expr()->eq('c.clientId', ':clientId'))
            ->setParameter(':clientId', $chat->getClientId())
            ->setMaxResults($limit);
        return $qb->getQuery()->getResult();
    }


    private function getBaseQueryChatsByDaysFromWeek()
    {
        return " FROM
        (SELECT 
            ADDDATE('1970-01-01', t4 * 10000 + t3 * 1000 + t2 * 100 + t1 * 10 + t0) gen_date
        FROM
            (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0, (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1, (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2, (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3, (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
    LEFT JOIN  (
    SELECT 
        COUNT(*) as total, DATE(start_time) as start_date
    FROM
        chats
    WHERE  DATE(start_time)  BETWEEN :start AND :end AND website = :website
    GROUP BY DATE(start_time)
    ) as chats ON gen_date =chats.start_date
    WHERE
        gen_date BETWEEN :start AND :end";
    }
}