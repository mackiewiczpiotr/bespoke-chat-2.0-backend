<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Chat\Report\Type;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class Website
 * @package AppBundle\Entity
 */
class Website
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $vatEmail;

    /**
     * @var string
     */
    private $livechatGroup;

    /**
     * @var string
     */
    private $livechatLicence;

    /**
     * @var DateTime
     *
     *
     */
    private $deleted;

    /**
     * @var Team
     */
    private $team;

    /**
     * @var Collection
     */
    private $chatReportTypes;

    /**
     * Website constructor.
     * @param $name
     * @param $livechatGroup
     * @param $livechatLicence
     */
    public function __construct($name, Team $team, $livechatGroup, $livechatLicence)
    {
        $this->name = $name;
        $this->team = $team;
        $this->livechatGroup = $livechatGroup;
        $this->livechatLicence = $livechatLicence;
        $this->deleted = NULL;
        $this->chatReportTypes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getVatEmail(): string
    {
        return $this->vatEmail;
    }

    /**
     * @param string $vatEmail
     */
    public function setVatEmail(string $vatEmail)
    {
        $this->vatEmail = $vatEmail;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Website
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function addChatReportType(Type $chatReportType)
    {
        $this->chatReportTypes->add($chatReportType);

        return $this;
    }

    public function removeChatReportType(Type $chatReportType)
    {
        $this->chatReportTypes->remove($chatReportType);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getChatReportTypes(): Collection
    {
        return $this->chatReportTypes;
    }


    /**
     * Get livechatId
     *
     * @return string
     */
    public function getLivechatGroup()
    {
        return $this->livechatGroup;
    }

    /**
     * Get livechatLicence
     *
     * @return string
     */
    public function getLivechatLicence()
    {
        return $this->livechatLicence;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    public function getFullName(){
        return $this->team->getName().' '.$this->name;
    }
}
