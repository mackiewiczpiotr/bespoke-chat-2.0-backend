<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Chat\Comment;
use AppBundle\Entity\Chat\Goal;
use AppBundle\Entity\Chat\History;
use AppBundle\Entity\Chat\Report;
use AppBundle\Entity\Chat\Survey;
use AppBundle\Entity\Chat\Variable;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


class Chat
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     *
     */
    private $livechatId;

    /**
     * @var string
     */
    private $clientName;

    /**
     * @var integer
     */
    private $spentTime;

    /**
     * @var DateTime
     *
     *
     */
    private $startTime;

    /**
     * @var DateTime
     *
     */
    private $endTime;

    /**
     * @var string
     *
     */
    private $rate;

    /**
     * @var string
     *
     */
    private $rateComment;


    /**
     * @var Website
     *
     */
    private $website;
    /**
     * @var Collection
     *
     */
    private $agents;

    /**
     * @var Collection
     *
     */
    private $tags;
    /**
     * @var Collection
     *
     */
    private $comments;
    /**
     * @var string
     *
     */
    private $startedSite;
    /**
     * @var string
     *
     */
    private $clientCountry;
    /**
     * @var integer
     *
     */
    private $clientIp;
    /**
     * @var string
     *
     */
    private $clientRefferer;
    /**
     * @var string
     *
     */
    private $clientCity;

    /**
     * @var string
     */
    private $clientUserAgent;

    /**
     * @var string
     *
     */
    private $clientId;
    /**
     * @var Collection
     */
    private $history;

    /**
     * @var Collection
     */
    private $goals;

    /**
     * @var Collection
     */
    private $surveys;

    /**
     * @var Collection
     */
    private $variables;

    /**
     * @var Collection
     */
    private $reports;

    /**
     * Chat constructor.
     * @param string $livechatId
     * @param string $clientName
     * @param DateTime $startTime
     * @param Collection $agents
     */
    public function __construct(Website $website, $livechatId, $clientName, DateTime $startTime, Collection $agents)
    {
        $this->website = $website;
        $this->livechatId = $livechatId;
        $this->clientName = $clientName;
        $this->startTime = $startTime;
        $this->agents = $agents;
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->history = new ArrayCollection();
        $this->goals = new ArrayCollection();
        $this->surveys = new ArrayCollection();
        $this->variables = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get livechatId
     *
     * @return string
     */
    public function getLivechatId()
    {
        return $this->livechatId;
    }

    /**
     * Set livechatId
     *
     * @param string $livechatId
     *
     * @return Chat
     */
    public function setLivechatId($livechatId)
    {
        $this->livechatId = $livechatId;

        return $this;
    }

    /**
     * Get clientName
     *
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * Set clientName
     *
     * @param string $clientName
     *
     * @return Chat
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;

        return $this;
    }

    /**
     * Get spentTime
     *
     * @return integer
     */
    public function getSpentTime()
    {
        return $this->spentTime;
    }

    /**
     * Set spentTime
     *
     * @param integer $spentTime
     *
     * @return Chat
     */
    public function setSpentTime($spentTime)
    {
        $this->spentTime = $spentTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set startTime
     *
     * @param DateTime $startTime
     *
     * @return Chat
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set endTime
     *
     * @param DateTime $endTime
     *
     * @return Chat
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return Chat
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Add agent
     *
     * @param User $agent
     *
     * @return Chat
     */
    public function addAgent(User $agent)
    {
        $this->agents[] = $agent;

        return $this;
    }

    /**
     * Remove agent
     *
     * @param User $agent
     */
    public function removeAgent(User $agent)
    {
        $this->agents->removeElement($agent);
    }

    /**
     * Get agents
     *
     * @return Collection
     */
    public function getAgents()
    {
        return $this->agents;
    }

    /**
     * Add tag
     *
     * @param Tag $tag
     *
     * @return Chat
     */
    public function addTag(Tag $tag)
    {
        $this->tags->add($tag);

        return $this;
    }

    /**
     * Remove tag
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get website
     *
     * @return Website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set website
     *
     * @param \AppBundle\Entity\Website $website
     *
     * @return Chat
     */
    public function setWebsite(\AppBundle\Entity\Website $website = null)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Add comment
     *
     * @param Comment $comment
     *
     * @return Chat
     */
    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param Comment $comment
     */
    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Get startedSite
     *
     * @return string
     */
    public function getStartedSite()
    {
        return $this->startedSite;
    }

    /**
     * Set startedSite
     *
     * @param string $startedSite
     *
     * @return Chat
     */
    public function setStartedSite($startedSite)
    {
        $this->startedSite = $startedSite;

        return $this;
    }

    /**
     * Get clientCountry
     *
     * @return string
     */
    public function getClientCountry()
    {
        return $this->clientCountry;
    }

    /**
     * Set clientCountry
     *
     * @param string $clientCountry
     *
     * @return Chat
     */
    public function setClientCountry($clientCountry)
    {
        $this->clientCountry = $clientCountry;

        return $this;
    }

    /**
     * Get clientIp
     *
     * @return integer
     */
    public function getClientIp()
    {
        return $this->clientIp;
    }

    /**
     * Set clientIp
     *
     * @param integer $clientIp
     *
     * @return Chat
     */
    public function setClientIp($clientIp)
    {
        $this->clientIp = $clientIp;

        return $this;
    }

    /**
     * Get clientRefferer
     *
     * @return string
     */
    public function getClientRefferer()
    {
        return $this->clientRefferer;
    }

    /**
     * Set clientRefferer
     *
     * @param string $clientRefferer
     *
     * @return Chat
     */
    public function setClientRefferer($clientRefferer)
    {
        $this->clientRefferer = $clientRefferer;

        return $this;
    }

    /**
     * Get clientCity
     *
     * @return string
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * Set clientCity
     *
     * @param string $clientCity
     *
     * @return Chat
     */
    public function setClientCity($clientCity)
    {
        $this->clientCity = $clientCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }


    /**
     * @param Chat\History $history
     * @return $this
     */
    public function addHistory(History $history)
    {
        $this->history->add($history);

        return $this;
    }

    /**
     * Remove $history
     *
     * @param History $history
     */
    public function removeHistory(History $history)
    {
        $this->history->removeElement($history);
    }

    /**
     * Get history
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @return Collection
     */
    public function getGoals()
    {
        return $this->goals;
    }

    public function addGoal(Goal $goal)
    {
        $this->goals->add($goal);

        return $this;
    }


    /**
     * @return Collection
     */
    public function getSurveys()
    {
        return $this->surveys;
    }

    public function addSurvey(Survey $survey)
    {
        $this->surveys->add($survey);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getVariables()
    {
        return $this->variables;
    }

    public function addVariable(Variable $variable)
    {
        $this->variables->add($variable);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getReports()
    {
        return $this->reports;
    }
    public function addReport(Report $report)
    {
        $this->reports->add($report);

        return $this;
    }

    /**
     * @return string
     */
    public function getClientUserAgent()
    {
        return $this->clientUserAgent;
    }

    /**
     * @param string $clientUserAgent
     */
    public function setClientUserAgent($clientUserAgent)
    {
        $this->clientUserAgent = $clientUserAgent;
    }

    /**
     * @return string
     */
    public function getRateComment()
    {
        return $this->rateComment;
    }

    /**
     * @param string $rateComment
     */
    public function setRateComment($rateComment)
    {
        $this->rateComment = $rateComment;
    }


    




}
