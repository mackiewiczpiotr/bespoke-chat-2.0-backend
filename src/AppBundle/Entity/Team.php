<?php

namespace AppBundle\Entity;
use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use Doctrine\Common\Collections\{
    ArrayCollection, Collection
};
use Exception;

/**
 * Team
 */
class Team
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Collection
     */
    private $members;

    /**
     * Constructor
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->members = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add member
     *
     * @param User $member
     * @return Team
     * @throws Exception
     */
    public function addMember(User $member)
    {
        if(!in_array(USER::$ROLE_AGENT,$member->getRoles())){
            throw new Exception('Member need\'s to be agent');
        }
        $this->members[] = $member;

        return $this;
    }

    /**
     * Remove member
     *
     * @param User $member
     */
    public function removeMember(User $member)
    {
        $this->members->removeElement($member);
    }

    /**
     * Get member
     *
     * @return Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

}
