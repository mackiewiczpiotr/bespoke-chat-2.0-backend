<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, \ArrayAccess
{
    public static $ROLE_TRAINER = 'ROLE_TRAINER';
    public static $ROLE_AGENT = 'ROLE_AGENT';
    public static $ROLE_PARTNER = 'ROLE_PARTNER';
    public static $ROLE_USER = 'ROLE_USER';
    public static $ROLE_ADMIN = 'ROLE_ADMIN';
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     *
     */
    private $username;


    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $contactPhone;

    /**
     * @var string
     */
    private $contactEmail;
    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var Website
     */
    private $lastWorkingContext;

    /**
     * Constructor
     */
    public function __construct($username, $email)
    {
        $this->isActive = true;
        $this->username = $username;
        $this->email = $email;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->roles;
        if (!in_array(self::$ROLE_USER, $roles)) {
            $roles[] = self::$ROLE_USER;
        }
        return $roles;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }


    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * Set contactPhone
     *
     * @param string $contactPhone
     *
     * @return User
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     *
     * @return User
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * @return Website
     */
    public function getLastWorkingContext()
    {
        return $this->lastWorkingContext;
    }

    /**
     * @param Website $lastWorkingContext
     */
    public function setLastWorkingContext(Website $lastWorkingContext)
    {
        $this->lastWorkingContext = $lastWorkingContext;
    }

    public function eraseCredentials()
    {

    }

    public function getName()
    {
        return !empty($this->firstName)? $this->firstName : $this->username;
    }

    public function getFullName(){
        if (!(empty($this->firstName) || empty($this->lastName))) {
            return $this->firstName . ' ' . $this->lastName;
        }
        return $this->username;
    }

    public function offsetExists($offset) {
        return isset($this->$offset);
    }

    public function offsetSet($offset, $value) {
        $this->$offset = $value;
    }

    public function offsetGet($offset) {
        return $this->$offset;
    }

    public function offsetUnset($offset) {
        $this->$offset = null;
    }
}
