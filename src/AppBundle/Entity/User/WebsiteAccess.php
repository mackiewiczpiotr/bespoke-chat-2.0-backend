<?php

namespace AppBundle\Entity\User;
use AppBundle\Entity\User;
use AppBundle\Entity\Website;

/**
 * WebsiteAccessRepository
 */
class WebsiteAccess
{
    const KAM = 'kam';
    const TRAINER = 'trainer';
    const OWNER = 'owner';
    const AGENT = 'agent';
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var Website
     */
    private $website;

    /**
     * @var User
     */
    private $user;

    /**
     * @var bool
     */
    private $primary;

    /**
     * WebsiteAccessRepository constructor.
     * @param string $type
     * @param Website $website
     * @param User $user
     */
    public function __construct(string $type, Website $website, User $user)
    {
        $this->setType($type);
        $this->website = $website;
        $this->user = $user;
        $this->primary = false;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Get website
     *
     * @return Website
     */
    public function getWebsite()
    {
        return $this->website;
    }


    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->primary;
    }

    /**
     * @param bool $primary
     * @return $this
     */
    public function setPrimary(bool $primary)
    {
        $this->primary = $primary;
        return $this;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type)
    {
        $availableTypes = [self::KAM,self::TRAINER,self::OWNER,self::AGENT];
        if(!in_array($type,$availableTypes)){
            throw new \DomainException("Unsupported access type $type");
        }
        $this->type = $type;
        return $this;
    }

}

