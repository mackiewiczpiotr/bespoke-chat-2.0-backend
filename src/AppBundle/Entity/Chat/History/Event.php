<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 12.02.18
 * Time: 20:10
 */

namespace AppBundle\Entity\Chat\History;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\History;
use AppBundle\Entity\User;
use DateTime;

class Event extends History
{

    /**
     * @var string
     */
    private $description;


    /**
     * Message constructor.
     * @param Chat $chat
     * @param DateTime $createdAt
     * @param $description
     */
    public function __construct(Chat $chat, DateTime $createdAt, $description)
    {
        $this->description = $description;
        parent::__construct($chat, $createdAt);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }




}
