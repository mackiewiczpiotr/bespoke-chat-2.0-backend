<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 12.02.18
 * Time: 20:11
 */

namespace AppBundle\Entity\Chat\History;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\History;
use DateTime;

class VisitedPage extends History
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $title;

    /**
     * VisitedPage constructor.
     * @param Chat $chat
     * @param DateTime $createdAt
     * @param $url
     * @param $title
     */
    public function __construct(Chat $chat,DateTime $createdAt,$url, $title)
    {
        $this->url = $url;
        $this->title = $title;
        parent::__construct($chat,$createdAt);
    }


    /**
     * Set pageUrl
     *
     * @param string $url
     *
     * @return VisitedPage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get pageUrl
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set pageTitle
     *
     * @param string $title
     *
     * @return VisitedPage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get pageTitle
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
