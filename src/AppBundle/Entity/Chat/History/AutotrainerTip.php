<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 25.02.18
 * Time: 15:00
 */

namespace AppBundle\Entity\Chat\History;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\History;
use AppBundle\Entity\User;
use DateTime;

class AutotrainerTip extends History
{

    const APPROVED = 'approved';
    const DECLINED = 'declined';
    /**
     * @var string
     */
    private $status;

    /**
     * @var User
     */
    private $statusAuthor;

    /**
     * @var string
     */
    private $message;

    /**
     * AutotrainerTip constructor.
     * @param Chat $chat
     * @param DateTime $createdAt
     * @param User $createdBy
     * @param $status
     */
    public function __construct(Chat $chat, DateTime $createdAt, $message)
    {
        $this->message = $message;

        parent::__construct($chat, $createdAt);
    }

    public function approve(User $author){
        $this->status = self::APPROVED;
        $this->statusAuthor = $author;
    }

    public function decline(User $author){
        $this->status = self::DECLINED;
        $this->statusAuthor = $author;
    }
}