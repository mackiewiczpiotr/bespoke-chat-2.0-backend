<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 12.02.18
 * Time: 20:10
 */

namespace AppBundle\Entity\Chat\History;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\History;
use AppBundle\Entity\User;
use DateTime;

class Message extends History
{

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $authorName;

    /**
     * @var User
     */
    private $agent;

    /**
     * Message constructor.
     * @param Chat $chat
     * @param DateTime $createdAt
     * @param string $text
     * @param string $authorName
     */
    public function __construct(Chat $chat, DateTime $createdAt, $text, $authorName)
    {
        $this->text = $text;
        $this->authorName = $authorName;
        parent::__construct($chat, $createdAt);
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get authorName
     *
     * @return string
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * Set authorName
     *
     * @param string $authorName
     *
     * @return Message
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;

        return $this;
    }

    /**
     * Get authorType
     *
     * @return string
     */
    public function getAuthorType()
    {
        return is_null($this->agent) ? 'visitor' : 'agent';
    }

    /**
     * @return User
     */
    public function getAgent(): User
    {
        return $this->agent;
    }

    /**
     * @param User $agent
     */
    public function setAgent(User $agent)
    {
        $this->agent = $agent;
    }



}
