<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 12.02.18
 * Time: 20:18
 */

namespace AppBundle\Entity\Chat\History;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\History;
use AppBundle\Entity\User;
use DateTime;

class TrainerTip extends History
{

    /**
     * @var string
     */
    private $message;

    /**
     * @var User
     */
    private $author;

    /**
     * TrainerTip constructor.
     * @param Chat $chat
     * @param DateTime $createdAt
     * @param User $createdBy
     * @param string $message
     */
    public function __construct(Chat $chat,DateTime $createdAt,User $createdBy, $message)
    {
        $this->message = $message;
        $this->author = $createdBy;

        parent::__construct($chat,$createdAt);
    }


    /**
     * Set message
     *
     * @param string $message
     *
     * @return TrainerTip
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set createdBy
     *
     * @param User $author
     *
     * @return TrainerTip
     */
    public function setAuthor(User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
