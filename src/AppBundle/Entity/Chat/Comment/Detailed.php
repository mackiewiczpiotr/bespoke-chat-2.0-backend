<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 30.04.18
 * Time: 09:29
 */

namespace AppBundle\Entity\Chat\Comment;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\Comment;
use AppBundle\Entity\Chat\History;
use AppBundle\Entity\User;

class Detailed extends Comment
{
    const CITATION = 'citation';
    const INFORMATION = 'information';
    const PRAISE = 'praise';
    const SELL = 'sell';
    const TECHNICAL = 'technical';
    const KNOWLEDGE = 'knowledge';

    /**
     * @var History
     */
    private $history;

    private $type;

    /**
     * @var User
     */
    private $recipient;

    public function __construct(Chat $chat, User $author, History $history,User $recipient, $type, $content)
    {
        parent::__construct($chat, $author, $content);
        $this->history = $history;
        $this->type = $type;
        $this->recipient = $recipient;
        $history->addComment($this);
    }

    /**
     * @return History
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return User
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

}