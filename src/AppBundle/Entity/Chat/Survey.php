<?php

namespace AppBundle\Entity\Chat;
use AppBundle\Entity\Chat;

/**
 * Survey
 */
class Survey
{
    const PRE_TYPE = 'pre';
    const POST_TYPE = 'post';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $question;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var Chat
     */
    private $chat;


    static function createPreSurvey(Chat $chat, $question, $answer){
        return new Survey( $chat, $question, $answer, self::PRE_TYPE);
    }

    static function createPostSurvey(Chat $chat, $question, $answer){
        return new Survey( $chat, $question, $answer, self::POST_TYPE);
    }
    /**
     * Survey constructor.
     * @param string $type
     * @param string $question
     * @param string $answer
     * @param Chat $chat
     */
    private function __construct(Chat $chat, $question, $answer,$type)
    {
        $this->type = $type;
        $this->question = $question;
        $this->answer = $answer;
        $this->chat = $chat;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }



    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Get chat
     *
     * @return Chat
     */
    public function getChat()
    {
        return $this->chat;
    }
}

