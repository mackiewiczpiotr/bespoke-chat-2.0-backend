<?php

namespace AppBundle\Entity\Chat;

use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\Report\State;
use AppBundle\Entity\Chat\Report\Type;
use AppBundle\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Report
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var DateTime
     */
    private $created;

    /**
     * @var string
     */
    private $clientName;

    /**
     * @var string
     */
    private $clientPhone;

    /**
     * @var string
     */
    private $clientEmail;

    /**
     * @var Chat
     */
    private $chat;

    /**
     * @var User
     */
    private $agent;

    /**
     * @var State
     */
    private $lastState;

    /**
     * @var Type
     *
     */
    private $reportType;

    /**
     * @var Collection
     */
    private $states;

    /**
     * @var string
     */
    private $description;

    /**
     * Report constructor.
     * @param string $id
     * @param DateTime $created
     * @param Chat $chat
     * @param User $agent
     * @param Type $chatReportType
     */
    public function __construct(string $id, DateTime $created, Chat $chat, User $agent, Type $chatReportType)
    {
        $this->id = $id;
        $this->created = $created;
        $this->chat = $chat;
        $this->agent = $agent;
        $this->states = new ArrayCollection();
        $this->reportType = $chatReportType;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get clientName
     *
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * Set clientName
     *
     * @param string $clientName
     *
     * @return Report
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;

        return $this;
    }

    /**
     * Get clientPhone
     *
     * @return string
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * Set clientPhone
     *
     * @param string $clientPhone
     *
     * @return Report
     */
    public function setClientPhone($clientPhone)
    {
        $this->clientPhone = $clientPhone;

        return $this;
    }

    /**
     * Get clientEmail
     *
     * @return string
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * Set clientEmail
     *
     * @param string $clientEmail
     *
     * @return Report
     */
    public function setClientEmail($clientEmail)
    {
        $this->clientEmail = $clientEmail;

        return $this;
    }

    /**
     * Get chat
     *
     * @return Chat
     */
    public function getChat()
    {
        return $this->chat;
    }

    /**
     * Get agent
     *
     * @return User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set agent
     *
     * @param User $agent
     *
     * @return Report
     */
    public function setAgent(User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get created
     *
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get lastState
     *
     * @return State
     */
    public function getLastState()
    {
        return $this->lastState;
    }

    /**
     * Get reportType
     *
     * @return Type
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @return Collection
     */
    public function getStates()
    {
        return $this->states;
    }

    public function addState(State $chatReportState)
    {
        $this->states->add($chatReportState);
        $this->lastState = $chatReportState;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }




    public function getName(){
        return '#'.$this->id;
    }


}
