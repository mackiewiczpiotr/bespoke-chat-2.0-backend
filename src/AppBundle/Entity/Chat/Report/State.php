<?php

namespace AppBundle\Entity\Chat\Report;
use AppBundle\Entity\Chat\Report;
use AppBundle\Entity\User;
use DateTime;

/**
 * State
 */
class State
{
    const READ = 'read';
    const ACCEPTED ='accepted';
    const CANCELLATION = 'cancellation';
    const REJECTED = 'rejected';
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $comment;
    /**
     * @var DateTime
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var Report
     */
    private $chatReport;

    /**
     * @var User
     */
    private $createdBy;


    /**
     * @param DateTime $createdAt
     * @param Report $chatReport
     * @return State
     */
    static function createReadStatus(DateTime $createdAt, Report $chatReport){
        return new State(self::READ,$createdAt,$chatReport);
    }

    /**
     * @param DateTime $createdAt
     * @param Report $chatReport
     * @return State
     */
    static function createAcceptedStatus(DateTime $createdAt, Report $chatReport){
        return new State(self::ACCEPTED,$createdAt,$chatReport);
    }

    /**
     * @param DateTime $createdAt
     * @param  $chatReport
     * @return State
     */
    static function createCancellationStatus(DateTime $createdAt, Report $chatReport){
        return new State(self::CANCELLATION,$createdAt,$chatReport);
    }

    /**
     * @param DateTime $createdAt
     * @param Report $chatReport
     * @return State
     */
    static function createRejectedStatus(DateTime $createdAt, Report $chatReport){
        return new State(self::REJECTED,$createdAt,$chatReport);
    }
    /**
     * State constructor.
     * @param string $state
     * @param DateTime $createdAt
     * @param Report $chatReport
     */
    private function __construct($state, DateTime $createdAt, Report $chatReport)
    {
        $this->state = $state;
        $this->createdAt = $createdAt;
        $this->chatReport = $chatReport;
    }

    /**
     * @param \AppBundle\Entity\User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }


    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get chatReport
     *
     * @return Report
     */
    public function getChatReport()
    {
        return $this->chatReport;
    }


    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }


}

