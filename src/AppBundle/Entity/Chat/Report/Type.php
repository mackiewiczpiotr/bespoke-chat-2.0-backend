<?php

namespace AppBundle\Entity\Chat\Report;

use AppBundle\Entity\Chat\Report\Type\Notification;
use AppBundle\Entity\Website;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Type
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Website
     */
    private $website;


    /**
     * @var Collection
     */
    private $notifications;

    /**
     * Type constructor.
     * @param string $name
     * @param string $description
     * @param Website $website
     */
    public function __construct($name, $description, Website $website)
    {
        $this->name = $name;
        $this->description = $description;
        $this->website = $website;
        $this->notifications = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Type
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Type
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get website
     *
     * @return Website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set website
     *
     * @param Website $website
     *
     * @return Type
     */
    public function setWebsite(Website $website = null)
    {
        $this->website = $website;

        return $this;
    }

    public function addNotification(Notification $notification)
    {
        $this->notifications->add($notification);
    }

    public function removeNotification(Notification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * @return Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
}
