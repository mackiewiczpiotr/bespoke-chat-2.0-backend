<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 05.01.18
 * Time: 19:33
 */

namespace AppBundle\Entity\Chat\Report\Type;


use AppBundle\Entity\Chat\Report\Type;

/**
 * Class Notification
 * @package AppBundle\Entity\Chat\Report\Type
 */
class Notification
{
    const EMAIL = 'email';
    const SMS = 'sms';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $value;

    /**
     * @var Type
     */
    private $report;

    /**
     * Notification constructor.
     * @param $type
     * @param $value
     * @param Type $report
     */
    private function __construct($type, $value, Type $report)
    {
        $this->type = $type;
        $this->value = $value;
        $this->report = $report;
    }

    /**
     * @param Type $report
     * @param $value
     * @return Notification
     */
    public static function createSmsNotification(Type $report, $value)
    {
        return new Notification(self::SMS, $value, $report);
    }

    /**
     * @param Type $type
     * @param $value
     * @return Notification
     */
    public static function createEmailNotification(Type $type, $value)
    {
        return new Notification(self::EMAIL, $value, $type);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return Type
     */
    public function getReport()
    {
        return $this->report;
    }

}