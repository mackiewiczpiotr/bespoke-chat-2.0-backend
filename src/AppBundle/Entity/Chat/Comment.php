<?php

namespace AppBundle\Entity\Chat;

use AppBundle\Entity\Chat;
use AppBundle\Entity\User;
use DateTime;

/**
 * ChatComment
 */
class Comment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var Chat
     */
    private $chat;
    /**
     * @var boolean
     */
    private $critical = false;

    /**
     * @var User
     */
    private $author;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * Comment constructor.
     * @param Chat $chat
     * @param User $author
     * @param string $content
     */
    public function __construct(Chat $chat, User $author, $content)
    {
        $this->chat = $chat;
        $this->author = $author;
        $this->content = $content;
        $this->createdAt = new DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get chat
     *
     * @return Chat
     */
    public function getChat()
    {
        return $this->chat;
    }


    /**
     * Get critical
     *
     * @return boolean
     */
    public function isCritical()
    {
        return $this->critical;
    }

    /**
     * Set critical
     *
     * @param boolean $critical
     *
     * @return Comment
     */
    public function setCritical($critical)
    {
        $this->critical = $critical;

        return $this;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

}
