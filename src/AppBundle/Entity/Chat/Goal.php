<?php

namespace AppBundle\Entity\Chat;
use AppBundle\Entity\Chat;

/**
 * Goal
 */
class Goal
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $livechatId;

    /**
     * @var \DateTime
     */
    private $achieved;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Chat
     */
    private $chat;

    /**
     * Goal constructor.
     * @param int $livechatId
     * @param \DateTime $achieved
     * @param string $name
     * @param Chat $chat
     */
    public function __construct(Chat $chat,$livechatId, \DateTime $achieved, $name)
    {
        $this->livechatId = $livechatId;
        $this->achieved = $achieved;
        $this->name = $name;
        $this->chat = $chat;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get livechatId
     *
     * @return integer
     */
    public function getLivechatId()
    {
        return $this->livechatId;
    }

    /**
     * Get achieved
     *
     * @return \DateTime
     */
    public function getAchieved()
    {
        return $this->achieved;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Get chat
     *
     * @return Chat
     */
    public function getChat()
    {
        return $this->chat;
    }
}

