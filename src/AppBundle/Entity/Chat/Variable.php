<?php

namespace AppBundle\Entity\Chat;
use AppBundle\Entity\Chat;

/**
 * Variable
 */
class Variable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $value;

    /**
     * @var Chat
     */
    private $chat;

    /**
     * Variable constructor.
     * @param string $key
     * @param string $value
     * @param Chat $chat
     */
    public function __construct(Chat $chat, $key, $value)
    {
        $this->key = $key;
        $this->value = $value;
        $this->chat = $chat;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }


    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get chat
     *
     * @return Chat
     */
    public function getChat()
    {
        return $this->chat;
    }
}

