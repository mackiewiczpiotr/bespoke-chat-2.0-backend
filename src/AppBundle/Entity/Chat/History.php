<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 12.02.18
 * Time: 20:09
 */

namespace AppBundle\Entity\Chat;


use AppBundle\Entity\Chat;
use AppBundle\Entity\Chat\Comment\Detailed;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

abstract class History
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var Chat
     */
    private $chat;

    /**
     * @var Collection
     */
    private $comments;

    /**
     * History constructor.
     * @param DateTime $createdAt
     * @param Chat $chat
     */
    public function __construct(Chat $chat, DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        $this->chat = $chat;
        $this->comments = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * Get chat
     *
     * @return Chat
     */
    public function getChat()
    {
        return $this->chat;
    }

    public function addComment(Detailed $comment){
        $this->comments->add($comment);
    }

    /**
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }


}
