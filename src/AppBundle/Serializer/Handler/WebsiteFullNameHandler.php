<?php
/**
 * Created by PhpStorm.
 * User: bartoszskuza
 * Date: 01/06/2018
 * Time: 23:07
 */

namespace AppBundle\Serializer\Handler;


use AppBundle\Entity\User;
use AppBundle\Entity\Website;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\Role;

class WebsiteFullNameHandler implements EventSubscriberInterface
{
    private $isPartner;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->isPartner = false;
        if(!is_null($tokenStorage->getToken())){
            $partnerRole = array_filter($tokenStorage->getToken()->getRoles(), function (Role $role) {
                return $role->getRole() === User::$ROLE_PARTNER;
            });
            $this->isPartner = count($partnerRole) > 0;
        }
    }

    /**
     * Returns the events to which this class has subscribed.
     *
     * Return format:
     *     array(
     *         array('event' => 'the-event-name', 'method' => 'onEventName', 'class' => 'some-class', 'format' => 'json'),
     *         array(...),
     *     )
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            ['event' => 'serializer.post_serialize',
                'class' => 'AppBundle\Entity\Website',
                'method' => 'onPostSerialize'],
        ];
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        $website = $event->getObject();
        if ($website instanceof Website) {
            $event->getVisitor()->setData('name',
                $this->isPartner ? $website->getName() : $website->getFullName());
        }

    }
}