#Bespokechat API

Aplikacja REST API stworzona przy pomocy:

* Symfony 3.3
  * FOSRestBundle
  * LexikJWTAuthenticationBundle
  * JMSSerializerBundle
* Doctrine

##Wymagania

* PHP 7.1
* MySQL 
* Pakiety wymagane do zainstalowania Symfony (które można sprawdzić przy pomocy `php ./bin/symfony_requirements`)

##Instalacja

Należy skorzystać ze standardowej instalacji projektu Symfony poprzez composer

####Wymagane parametry (`parameters.yaml`)
* database_* - standardowe parametry wymagane przez Doctrine do połączenia się z bazą danych
* jwt_token_ttl - ilość sekund po jakiej token autoryzacyjny straci ważność
* jwt_key_pass_phrase, jwt_private_key_path, jwt_public_key_path - informacje związane z kluczem ssh wymaganym przez LexikJWTAuthenticationBundle. Intrukcja w jaki sposób wygnerować taki klucz znajduje sie na [stronie bundla](
https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#installation)
* lc_login, lc_password - dane logowania do API livechatu
* excluded_agents - tablica maili agentów, którzy nie powinni wyświetlać się w liście osób, które obsługują czat

## Uruchamianie aplikacji na środowisku deweloperskim
W celu szybkiego uzupełniania bazy danych testowymi danymi można skorzystać z komendy `php ./bin/console doctrine:fixture:load`. Ze względów wydajnościowych proces ładowania rekordów podzielono na mniejszce części. ***Podczas dodawanie nowych funkcjonalności również powinny powstać nowe Fixtury w celu ułatwienia pracy na środowisku lokalnym***

## Architektura

Aplikacja została stworzona z myślą o API w postaci REST zwracający odpowiedź w formacie JSON. Pakiet FOSRestBundle [ułatwia tworzenie akcji zgodnych z architekturą REST](https://symfony.com/doc/current/bundles/FOSRestBundle/5-automatic-route-generation_single-restful-controller.html#define-resource-actions).

### Encje
Klasy encji zostały stworzone jako POPO (Plain Old PHP Object) w związku z tym ***niedozowlone*** jest używanie adnotacji w klasach domenowych. 

###Serializacja 

Aplikacja korzysta z JMSSerializerBundle w celu ułatwienia serializacji danych do formatu JSON.

#### Encje
Każda encja, która jest zwracana powinna posiadać swój plik konfiguracyjny(`Resources/config/serializer/`), gdzie domyślnie ***wszystkie właściwości powinny być wyłączone z procesu serializacji (`exclusion_policy: ALL`)***, a następnie jawnie wyeksponowane. Pozwoli to na wyeliminownie przypadkowego wyeksportowania danych, których nie powinno zwracać API dla danej encji

#### Grupy
W celu zdefiniowanie różnych serializacji dla tej samej encji (np. w sytuacji serializacji podstawowych danych wymaganych do wyświetlania listy oraz danych szczegółowych prezentowanych jako podsumowanie encji) należy skorzystać z [mechanizmu grup](http://jmsyst.com/libs/serializer/master/cookbook/exclusion_strategies#creating-different-views-of-your-objects) oferowanych przez JMSSerializer.

***Należy pamiętać, że domyślną grupą jest `Default` i jeżeli chcemy udostępnić domyślnie wyeksportowane dane jak i te z jakiejś grupy należy przekazać do kontekstu serializacji zarówno grupę domyślną jak i szczegółową***.       

#### Skomplikowane struktry
W przypadku, gdy API ma zwrócić obiekt, który zawiera w sobie różne encje JSMSeriazlier, może nieprawidłowo interpretować kontekst serializacji. W takim przypadku należy stworzyć Data Transfer Object (przykład: `AppBundle\DTO\WebsiteDetails`)

#### Modifkowanie serializacji w zależności od Roli
Należy stworzyć Handler implementujący intefejs `JMS\Serializer\EventDispatcher\EventSubscriberInterface` a następnie dodać go do `services.yml` z tagiem `jms_serializer.event_subscriber` (przykład: `AppBundle\Serializer\Handler\WebsiteFullNameHandler`).
***Proszę zwrócić uwagę, że taki handler będzie działał globalnie w obrębie aplikacji***.

### Paginacja

W przypadku tworzenie endpointów odpowiedzialnych za zwracanie kolekcji danych z możliwością dodatkowej filtracji/sortowania należy skorzystać z `FOS\RestBundle\Request\ParamFetcher` odpowiedzialnego za filtrowanie poprawnych danych pobieranych z żądania oraz `AppBundle\Pagination\Pagnator` jako klasę dostosowywująca bazowe zapytanie do bazy danych zgodnie z otrzymanym żadaniem.
